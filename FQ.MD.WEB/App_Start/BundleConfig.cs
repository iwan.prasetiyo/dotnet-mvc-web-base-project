﻿using System.Web;
using System.Web.Optimization;

namespace FQ.MD.WEB
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/jquery-ui.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js",
                      "~/Scripts/DataTables/jquery.dataTables.min.js",
                      "~/Scripts/DataTables/dataTables.checkboxes.min.js",
                      "~/Scripts/DataTables/dataTables.buttons.min.js",
                      "~/Scripts/DataTables/buttons.flash.min.js",
                      "~/Scripts/DataTables/buttons.print.min.js",
                      "~/Scripts/DataTables/buttons.html5.min.js",
                      "~/Scripts/select2.min.js",
                      "~/Scripts/jquery-ui.multidatespicker.js",
                      "~/Scripts/Chart.min.js"));

            bundles.Add(new StyleBundle("~/CSSBundle").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/fontawesome-all.css",
                      "~/Content/jquery-ui.css",
                      "~/Content/site.css",
                      "~/Content/DataTables/css/jquery.dataTables.min.css",
                      "~/Content/DataTables/css/dataTables.checkboxes.css",
                      "~/Content/DataTables/css/buttons.dataTables.min.css",
                      "~/Content/css/select2.min.css",
                      "~/Content/jquery-ui.multidatespicker.css",
                      "~/Content/Chart.min.css"));
        }
    }
}
