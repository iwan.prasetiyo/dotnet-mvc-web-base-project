//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FQ.MD.WEB.Models
{
    using System;
    using System.Collections.Generic;

    public partial class GT_MASTER_MENUS
    {
        public GT_MASTER_MENUS()
        {
            this.GT_MASTER_MENUS1 = new HashSet<GT_MASTER_MENUS>();
            this.GT_MASTER_ROLE_MENUS = new HashSet<GT_MASTER_ROLE_MENUS>();
        }

        public int MenuId { get; set; }
        public string Key { get; set; }
        public string Description { get; set; }
        public string URL { get; set; }
        public string Icon { get; set; }
        public int Sequence { get; set; }
        public Nullable<int> ParentId { get; set; }
        public bool IsLink { get; set; }
        public bool IsActive { get; set; }
        public Nullable<int> InactivatedBy { get; set; }
        public Nullable<System.DateTime> InactivatedDate { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> LastUpdatedBy { get; set; }
        public Nullable<System.DateTime> LastUpdatedDate { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<int> DeletedBy { get; set; }
        public Nullable<System.DateTime> DeletedDate { get; set; }
        public string Remark { get; set; }

        public virtual ICollection<GT_MASTER_MENUS> GT_MASTER_MENUS1 { get; set; }
        public virtual GT_MASTER_MENUS GT_MASTER_MENUS2 { get; set; }
        public virtual ICollection<GT_MASTER_ROLE_MENUS> GT_MASTER_ROLE_MENUS { get; set; }
    }
}
