﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace FQ.MD.WEB.Helpers
{
    public static class CSVHelpers
    {
        public static string Read(String csvFilePath)
        {
            using (var stream = new FileStream(csvFilePath, FileMode.Open))
            {
                var reader = new StreamReader(stream);
                stream.Seek(0, SeekOrigin.Begin);
                return reader.ReadToEnd();
            }
        }
    }
}