﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FQ.MD.WEB.Helpers
{
    public static class PasswordHelpers
    {
        public static void GeneratePassword(string Password, out string EncriptedPassword, out string Salt)
        {
            var crypto = new SimpleCrypto.PBKDF2();
            var encryppass = crypto.Compute(Password);

            EncriptedPassword = crypto.Compute(Password);
            Salt = crypto.Salt;
        }
    }
}
