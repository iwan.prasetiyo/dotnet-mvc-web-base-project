﻿using FQ.MD.WEB.Resources;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.SessionState;

namespace FQ.MD.WEB.Helpers
{
    public class CultureHelper
    {
        protected HttpSessionState session;

        public CultureHelper(HttpSessionState httpSessionState)
        {
            session = httpSessionState;
        }

        public static int CurrentCulture
        {
            get
            {
                if (Thread.CurrentThread.CurrentUICulture.Name == "id-ID")
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                if (value == 1)
                {
                    //** Set decimal separator**//
                    var culture = new CultureInfo("id-ID");
                    culture.NumberFormat.NumberDecimalSeparator = ".";

                    Thread.CurrentThread.CurrentUICulture = culture;
                }
                else
                {
                    Thread.CurrentThread.CurrentUICulture = CultureInfo.InvariantCulture;
                }

                Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture;

            }
        }


        public static string GetResourceByCultureId(string key, int cultureId)
        {
            var culture = CultureInfo.InvariantCulture;
            if (cultureId == 1)
            {
                culture = new CultureInfo("id-ID");
            }
            return Res.ResourceManager.GetString(key, culture);
        }
    }
}