﻿using FQ.MD.WEB.Models;
using FQ.MD.WEB.Services;
using FQ.MD.WEB.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FQ.MD.WEB.Helpers
{
    public class MenuHelpers
    {
        private MenuServices _menuServices = new MenuServices();

        public List<MenuItem> GetMenuFromUser(GT_MASTER_USERS user, string Controller)
        {
            var menus = from ur in user.GT_MASTER_USER_ROLES.Where(q => q.IsActive && !q.IsDeleted)
                        from rm in ur.GT_MASTER_ROLES.GT_MASTER_ROLE_MENUS.Where(q => q.IsActive && !q.IsDeleted)
                        where rm.GT_MASTER_ROLES.IsActive && !rm.GT_MASTER_ROLES.IsDeleted && rm.GT_MASTER_MENUS.IsActive && !rm.GT_MASTER_MENUS.IsDeleted
                        select rm.GT_MASTER_MENUS;

            var displayMenus = new List<MenuItem>();
            foreach (var menu in menus)
            {
                if (menu.ParentId.HasValue && displayMenus.Where(q => q.MenuId == menu.ParentId.Value).Count() == 0)
                {
                    var parent = _menuServices.GetMenuById(menu.ParentId.Value);
                    displayMenus.Add(new MenuItem()
                    {
                        MenuId = parent.MenuId,
                        Menu = parent.Key,
                        Display = "<div class=\"menu-icon\"><i class=\"" + parent.Icon + "\"></i></div>" + CultureHelper.GetResourceByCultureId(parent.Key, CultureHelper.CurrentCulture),
                        Sequence = parent.Sequence,
                        IsActive = menus.Where(q => q.ParentId == parent.MenuId && q.IsActive && ("/" + Controller).StartsWith(q.URL)).Count() > 0
                    });
                }

                displayMenus.Add(new MenuItem
                {
                    Url = menu.URL,
                    Menu = menu.Key,
                    ParentMenuId = menu.ParentId,
                    Display = "<div class=\"menu-icon\"><i class=\"" + menu.Icon + "\"></i></div>" + CultureHelper.GetResourceByCultureId(menu.Key, CultureHelper.CurrentCulture),
                    Sequence = menu.Sequence,
                    IsActive = ("/" + Controller).StartsWith(menu.URL)
                });
            }

            return displayMenus.OrderBy(q => q.Sequence).ToList();
        }

        public AuthorizationResult IsAuthorized(GT_MASTER_USERS user, string Controller, string Action)
        {
            var result = new AuthorizationResult() { IsAthorized = false };

            var menus = from ur in user.GT_MASTER_USER_ROLES.Where(q => q.IsActive && !q.IsDeleted)
                        from rm in ur.GT_MASTER_ROLES.GT_MASTER_ROLE_MENUS.Where(q => q.IsActive && !q.IsDeleted)
                        where rm.GT_MASTER_ROLES.IsActive && !rm.GT_MASTER_ROLES.IsDeleted && rm.GT_MASTER_MENUS.IsActive && !rm.GT_MASTER_MENUS.IsDeleted
                        select rm.GT_MASTER_MENUS;

            var isMenuGranted = menus.Where(q => ("/" + Controller).StartsWith(q.URL)).Count() > 0;
            if (isMenuGranted)
            {
                var roleMenus = from ur in user.GT_MASTER_USER_ROLES.Where(q => q.IsActive && !q.IsDeleted)
                                from rm in ur.GT_MASTER_ROLES.GT_MASTER_ROLE_MENUS.Where(q => q.IsActive && !q.IsDeleted)
                                where ("/" + Controller).StartsWith(rm.GT_MASTER_MENUS.URL)
                                select rm;

                var isActionGranted = Action.Contains("Add") ? roleMenus.Where(q => q.GrantCreate).Count() > 0 :
                    Action.Contains("Edit") ? roleMenus.Where(q => q.GrantUpdate).Count() > 0 :
                    Action.Contains("Delete") ? roleMenus.Where(q => q.GrantDelete).Count() > 0 :
                    Action.Contains("Detail") || Action.Contains("Index") ? roleMenus.Where(q => q.GrantRead).Count() > 0 :
                    Action.Contains("Sync") ? roleMenus.Where(q => q.GrantSync).Count() > 0 :
                    Action.Contains("Upload") ? roleMenus.Where(q => q.GrantUpload).Count() > 0 :
                    false;
                if (isActionGranted)
                {
                    var menuIdAccessed = menus.Where(q => q.URL.ToLower().Contains(Controller.ToLower())).Select(q => q.MenuId).FirstOrDefault();
                    result.IsAthorized = true;
                    result.IsCreateGranted = roleMenus.Where(q => q.GrantCreate).Count() > 0;
                    result.IsReadGranted = roleMenus.Where(q => q.GrantRead).Count() > 0;
                    result.IsUpdateGranted = roleMenus.Where(q => q.GrantUpdate).Count() > 0;
                    result.IsDeleteGranted = roleMenus.Where(q => q.GrantDelete).Count() > 0;
                    result.IsSyncGranted = roleMenus.Where(q => q.GrantSync).Count() > 0;
                    result.IsUploadGranted = roleMenus.Where(q => q.GrantUpload).Count() > 0;

                    result.MenuId = menuIdAccessed;
                    return result;
                }
            }

            return result;
        }
    }
}