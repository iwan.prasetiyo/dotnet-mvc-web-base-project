﻿using FQ.MD.WEB.ViewModels.JSON;
using FQ.MD.WEB.Resources;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace FQ.MD.WEB.Helpers
{
    public static class JsonHelpers<TModel> where TModel : class
    {
        public static byte[] ToJsonBytes(TModel model)
        {
            return new ASCIIEncoding().GetBytes(new JavaScriptSerializer().Serialize(model));
        }

        public static TModel StringToModel(string json)
        {
            return new JavaScriptSerializer().Deserialize<TModel>(json);
        }

        public static RestResponse<TModel> GetHttpJson(string Url, string Method, bool IsAuth, string Token, TModel Data)
        {
            var result = new RestResponse<TModel>() { IsSuccess = false };

            var request = WebRequest.Create(Url) as HttpWebRequest;
            request.Method = Method;
            request.ContentType = "application/json";

            if (IsAuth && !string.IsNullOrEmpty(Token))
            {
                request.Headers.Add("Authorization", "Basic " + Token);
            }

            if (Method == "POST" || Method == "PUT")
            {
                var jsonData = JsonHelpers<TModel>.ToJsonBytes(Data);

                request.ContentLength = jsonData.Length;
                request.GetRequestStream().Write(jsonData, 0, jsonData.Length);
            }

            var response = request.GetResponse() as HttpWebResponse;
            if ((int)response.StatusCode == 200)
            {
                result = JsonHelpers<RestResponse<TModel>>.StringToModel(new StreamReader(response.GetResponseStream()).ReadToEnd());
            }
            else
            {
                result.Message = Res.HttpRequestError;
            }

            return result;
        }
    }
}
