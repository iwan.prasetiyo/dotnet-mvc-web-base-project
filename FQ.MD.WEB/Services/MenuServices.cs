﻿using FQ.MD.WEB.DAL;
using FQ.MD.WEB.Models;
using FQ.MD.WEB.ViewModels.Form;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FQ.MD.WEB.Services
{
    public class MenuServices : BaseServices
    {
        private UnitOfWork _unitOfWork = new UnitOfWork();
        private LogErrorServices _errorLogServices = new LogErrorServices();

        public IEnumerable<GT_MASTER_MENUS> Get(
            Expression<Func<GT_MASTER_MENUS, bool>> filter = null,
            Func<IQueryable<GT_MASTER_MENUS>, IOrderedQueryable<GT_MASTER_MENUS>> orderBy = null,
            int? skip = null,
            int? take = null)
        {
            Expression<Func<GT_MASTER_MENUS, bool>> isNotDeleted = u => !u.IsDeleted;
            if (filter != null)
            {
                filter = filter.AndAlso(isNotDeleted);
            }
            else
            {
                filter = isNotDeleted;
            }
            return _unitOfWork.MenuRepository.Get(filter: filter, orderBy: orderBy, skip: skip, take: take);
        }

        public GT_MASTER_MENUS GetMenuById(int Id)
        {
            Expression<Func<GT_MASTER_MENUS, bool>> filter = u => !u.IsDeleted && u.MenuId == Id;
            return _unitOfWork.MenuRepository.Get(filter: filter).FirstOrDefault();
        }

        public ServiceResult AddMenu(MenuAddForm form, GT_MASTER_USERS actionUser)
        {
            var res = new ServiceResult() { IsSuccess = false };
            try
            {
                var model = new GT_MASTER_MENUS();
                model.Key = form.Key;
                model.Description = form.Description;
                model.URL = form.Url;
                model.Icon = form.Icon;
                model.Sequence = form.Sequence;
                model.ParentId = form.ParentId;
                model.IsLink = string.IsNullOrEmpty(form.Url) ? false : true;
                model.IsActive = form.IsActive;

                model.CreatedBy = actionUser.UserId;
                model.CreatedDate = DateTime.Now;

                _unitOfWork.MenuRepository.Insert(model);
                _unitOfWork.Save();

                res.IsSuccess = true;
            }
            catch (Exception e)
            {
                var log = _errorLogServices.AddLog(e, actionUser.UserId);

                res.IsSuccess = false;
                res.Message = log.Message;
            }
            return res;
        }

        public ServiceResult UpdateMenu(MenuEditForm form, GT_MASTER_USERS actionUser)
        {
            var res = new ServiceResult() { IsSuccess = false };

            var model = this.GetMenuById(form.MenuId);
            if (model == null)
            {
                res.Message = "ValidationIdNotFound";
                return res;
            }

            try
            {
                //model.Key = form.Key;
                model.Description = form.Description;
                model.URL = form.Url;
                model.Icon = form.Icon;
                model.Sequence = form.Sequence;
                model.ParentId = form.ParentId;
                model.IsLink = string.IsNullOrEmpty(form.Url) ? false : true;

                model.IsActive = form.IsActive;
                if (!form.IsActive)
                {
                    model.InactivatedBy = actionUser.UserId;
                    model.InactivatedDate = DateTime.Now;
                }

                model.LastUpdatedBy = actionUser.UserId;
                model.LastUpdatedDate = DateTime.Now;

                _unitOfWork.MenuRepository.Update(model);
                _unitOfWork.Save();

                res.IsSuccess = true;
            }
            catch (Exception e)
            {
                var log = _errorLogServices.AddLog(e, model.CreatedBy);

                res.IsSuccess = false;
                res.Message = log.Message;
            }
            return res;
        }

        public ServiceResult DeleteMenu(MenuDeleteForm form, GT_MASTER_USERS actionUser)
        {
            var res = new ServiceResult() { IsSuccess = false };

            var model = this.GetMenuById(form.MenuId);
            if (model == null)
            {
                res.Message = "ValidationIdNotFound";
                return res;
            }

            try
            {
                model.IsDeleted = true;
                model.DeletedBy = actionUser.UserId;
                model.DeletedDate = DateTime.Now;

                model.LastUpdatedBy = actionUser.UserId;
                model.LastUpdatedDate = DateTime.Now;

                _unitOfWork.MenuRepository.Update(model);
                _unitOfWork.Save();

                res.IsSuccess = true;
            }
            catch (Exception e)
            {
                var log = _errorLogServices.AddLog(e, model.CreatedBy);

                res.IsSuccess = false;
                res.Message = log.Message;
            }
            return res;
        }
    }
}