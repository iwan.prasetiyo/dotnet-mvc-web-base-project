﻿using FQ.MD.WEB.DAL;
using FQ.MD.WEB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FQ.MD.WEB.Services
{
    public class LogErrorServices
    {
        private UnitOfWork _unitOfWork = new UnitOfWork();

        public string GetExceptionMessageKey(Exception exception)
        {
            var errorLog = _unitOfWork.LogErrorRepository.Get(d => d.ErrorCode == exception.HResult).FirstOrDefault();
            return errorLog == null ? "ServerErrorGeneric" : errorLog.DisplayKey;
        }

        public ServiceResult AddLog(Exception exception, int? UserId)
        {
            var result = new ServiceResult() { IsSuccess = false };
            var baseException = exception.GetBaseException();

            try
            {
                var displayKey = GetExceptionMessageKey(baseException);

                var model = new GT_LOG_APP_ERRORS();
                model.ErrorCode = baseException.HResult;
                model.ErrorMessage = baseException.Message;
                model.DisplayKey = displayKey;
                model.CreatedBy = UserId;
                model.CreatedDate = DateTime.Now;

                _unitOfWork.LogErrorRepository.Insert(model);
                _unitOfWork.Save();

                result.IsSuccess = true;
                result.Message = displayKey;
            }
            catch (Exception e)
            {
                result.Message = GetExceptionMessageKey(e);
            }

            return result;
        }

        public GT_LOG_APP_ERRORS GetErrorLogById(int Id)
        {
            return _unitOfWork.LogErrorRepository.GetByID(Id);
        }

        public GT_LOG_APP_ERRORS GetLastError()
        {
            return _unitOfWork.LogErrorRepository.Get(orderBy: q => q.OrderByDescending(u => u.Id)).FirstOrDefault();
        }
    }
}
