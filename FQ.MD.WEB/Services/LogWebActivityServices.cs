﻿using FQ.MD.WEB.DAL;
using FQ.MD.WEB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FQ.MD.WEB.Services
{
    public class LogWebActivityServices
    {
        private UnitOfWork _unitOfWork = new UnitOfWork();
        private LogErrorServices _errorLogServices = new LogErrorServices();

        public IEnumerable<GT_LOG_WEB_ACTIVITIES> Get(
            Expression<Func<GT_LOG_WEB_ACTIVITIES, bool>> filter = null,
            Func<IQueryable<GT_LOG_WEB_ACTIVITIES>, IOrderedQueryable<GT_LOG_WEB_ACTIVITIES>> orderBy = null,
            int? take = null,
            int? start = null
            )
        {
            return _unitOfWork.LogWebActivityRepository.Get(filter: filter, orderBy: orderBy, skip: start, take: take);
        }

        public ServiceResult AddLog(GT_LOG_WEB_ACTIVITIES model)
        {
            var result = new ServiceResult() { IsSuccess = false };

            try
            {
                _unitOfWork.LogWebActivityRepository.Insert(model);
                _unitOfWork.Save();

                result.IsSuccess = true;
            }
            catch (Exception e)
            {
                var log = _errorLogServices.AddLog(e, model.UserId);
                result.Message = log.Message;
            }

            return result;
        }
    }
}
