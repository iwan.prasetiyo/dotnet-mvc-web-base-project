﻿using FQ.MD.WEB.DAL;
using FQ.MD.WEB.Models;
using FQ.MD.WEB.ViewModels.Form;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FQ.MD.WEB.Services
{
    public class StatusServices : BaseServices
    {
        private UnitOfWork _unitOfWork = new UnitOfWork();
        private LogErrorServices _errorLogServices = new LogErrorServices();

        public IEnumerable<GT_MASTER_STATUS> Get(
            Expression<Func<GT_MASTER_STATUS, bool>> filter = null,
            Func<IQueryable<GT_MASTER_STATUS>, IOrderedQueryable<GT_MASTER_STATUS>> orderBy = null,
            int? skip = null,
            int? take = null)
        {
            Expression<Func<GT_MASTER_STATUS, bool>> isNotDeleted = u => !u.IsDeleted;
            if (filter != null)
            {
                filter = filter.AndAlso(isNotDeleted);
            }
            else
            {
                filter = isNotDeleted;
            }

            return _unitOfWork.StatusRepository.Get(filter: filter, orderBy: orderBy, skip: skip, take: take);
        }

        public GT_MASTER_STATUS GetStatusById(int Id)
        {
            Expression<Func<GT_MASTER_STATUS, bool>> filter = u => !u.IsDeleted && u.StatusId == Id;
            return _unitOfWork.StatusRepository.Get(filter: filter).FirstOrDefault();
        }

        public ServiceResult AddStatus(StatusAddForm form, GT_MASTER_USERS actionUser)
        {
            var res = new ServiceResult() { IsSuccess = false };
            try
            {
                var model = new GT_MASTER_STATUS();
                model.ModuleId = form.ModuleId;
                model.Status = form.Status.ToUpper();
                model.Description = form.Description;
                model.CreatedBy = actionUser.UserId;
                model.CreatedDate = DateTime.Now;

                _unitOfWork.StatusRepository.Insert(model);
                _unitOfWork.Save();
                res.IsSuccess = true;
            }
            catch (Exception e)
            {
                var log = _errorLogServices.AddLog(e, actionUser.UserId);
                res.IsSuccess = false;
                res.Message = log.Message;
            }
            return res;
        }

        public ServiceResult UpdateStatus(StatusEditForm form, GT_MASTER_USERS actionUser)
        {
            var res = new ServiceResult() { IsSuccess = false };

            var model = this.GetStatusById(form.StatusId);
            if (model == null)
            {
                res.Message = "ValidationIdNotFound";
                return res;
            }

            try
            {
                //model.Status = form.Status;
                model.Description = form.Description;
                model.LastUpdatedBy = actionUser.UserId;
                model.LastUpdatedDate = DateTime.Now;

                _unitOfWork.StatusRepository.Update(model);
                _unitOfWork.Save();
                res.IsSuccess = true;
            }
            catch (Exception e)
            {
                var log = _errorLogServices.AddLog(e, actionUser.UserId);
                res.IsSuccess = false;
                res.Message = log.Message;
            }
            return res;
        }

        public ServiceResult DeletedStatus(StatusDeleteForm form, GT_MASTER_USERS actionUser)
        {
            var res = new ServiceResult() { IsSuccess = false };

            var model = this.GetStatusById(form.StatusId);
            if (model == null)
            {
                res.Message = "ValidationIdNotFound";
                return res;
            }

            try
            {
                model.IsDeleted = true;
                model.DeletedBy = actionUser.UserId;
                model.DeletedDate = DateTime.Now;
                model.LastUpdatedBy = actionUser.UserId;
                model.LastUpdatedDate = DateTime.Now;

                _unitOfWork.StatusRepository.Update(model);
                _unitOfWork.Save();
                res.IsSuccess = true;
            }
            catch (Exception e)
            {
                var log = _errorLogServices.AddLog(e, actionUser.UserId);
                res.IsSuccess = false;
                res.Message = log.Message;
            }
            return res;
        }
    }
}
