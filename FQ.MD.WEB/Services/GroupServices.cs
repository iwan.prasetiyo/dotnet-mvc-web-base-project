﻿using FQ.MD.WEB.DAL;
using FQ.MD.WEB.Models;
using FQ.MD.WEB.ViewModels.Form;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System;
using FQ.MD.WEB.Resources;

namespace FQ.MD.WEB.Services
{
    public class GroupServices
    {
        private UnitOfWork _unitOfWork = new UnitOfWork();
        private LogErrorServices _errorLogServices = new LogErrorServices();

        public IEnumerable<GT_MASTER_GROUPS> Get(
            Expression<Func<GT_MASTER_GROUPS, bool>> filter = null,
            Func<IQueryable<GT_MASTER_GROUPS>, IOrderedQueryable<GT_MASTER_GROUPS>> orderBy = null,
            int? skip = null,
            int? take = null)
        {
            Expression<Func<GT_MASTER_GROUPS, bool>> isNotDeleted = u => !u.IsDeleted;
            if (filter != null)
            {
                filter = filter.AndAlso(isNotDeleted);
            }
            else
            {
                filter = isNotDeleted;
            }

            return _unitOfWork.GroupRepository.Get(filter: filter, orderBy: orderBy, skip: skip, take: take);
        }

        public GT_MASTER_GROUPS GetGroupById(int Id)
        {
            Expression<Func<GT_MASTER_GROUPS, bool>> filter = u => !u.IsDeleted && u.GroupId == Id;
            return _unitOfWork.GroupRepository.Get(filter: filter).FirstOrDefault();
        }

        public ServiceResult AddGroup(GroupAddForm form, GT_MASTER_USERS actionUser)
        {
            var res = new ServiceResult() { IsSuccess = false };
            try
            {
                var model = new GT_MASTER_GROUPS();
                model.GroupName = form.GroupName.ToUpper();
                model.CompanyId = form.CompanyId;
                model.ParentId = form.ParentId;
                model.Note = form.Note;
                model.IsActive = form.IsActive;
                model.CreatedBy = actionUser.UserId;
                model.CreatedDate = DateTime.Now;

                _unitOfWork.GroupRepository.Insert(model);
                _unitOfWork.Save();

                res.IsSuccess = true;
            }
            catch (Exception e)
            {
                var log = _errorLogServices.AddLog(e, actionUser.UserId);

                res.IsSuccess = false;
                res.Message = log.Message;
            }
            return res;
        }

        public ServiceResult UpdateGroup(GroupEditForm form, GT_MASTER_USERS actionUser)
        {
            var res = new ServiceResult() { IsSuccess = false };

            var model = this.GetGroupById(form.GroupId);
            if (model == null)
            {
                res.Message = "ValidationIdNotFound";
                return res;
            }

            try
            {
                //model.GroupName = viewModel.GroupName;
                //model.CompanyId = form.CompanyId;
                model.ParentId = form.ParentId;
                model.Note = form.Note;
                model.IsActive = form.IsActive;
                if (!form.IsActive)
                {
                    model.InactivatedBy = actionUser.UserId;
                    model.InactivatedDate = DateTime.Now;
                }
                model.LastUpdatedBy = actionUser.UserId;
                model.LastUpdatedDate = DateTime.Now;

                _unitOfWork.GroupRepository.Update(model);
                _unitOfWork.Save();

                res.IsSuccess = true;
            }
            catch (Exception e)
            {
                var log = _errorLogServices.AddLog(e, model.CreatedBy);

                res.IsSuccess = false;
                res.Message = log.Message;
            }
            return res;
        }

        public ServiceResult DeleteGroup(GroupDeleteForm form, GT_MASTER_USERS actionUser)
        {
            var res = new ServiceResult() { IsSuccess = false };

            var model = this.GetGroupById(form.GroupId);
            if (model == null)
            {
                res.Message = "ValidationIdNotFound";
                return res;
            }

            try
            {
                model.Remark = form.Remark;
                model.IsDeleted = true;
                model.DeletedBy = actionUser.UserId;
                model.DeletedDate = DateTime.Now;
                model.LastUpdatedBy = actionUser.UserId;
                model.LastUpdatedDate = DateTime.Now;

                _unitOfWork.GroupRepository.Update(model);
                _unitOfWork.Save();

                res.IsSuccess = true;
            }
            catch (Exception e)
            {
                var log = _errorLogServices.AddLog(e, model.CreatedBy);

                res.IsSuccess = false;
                res.Message = log.Message;
            }
            return res;
        }
    }
}
