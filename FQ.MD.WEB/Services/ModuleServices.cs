﻿using FQ.MD.WEB.DAL;
using FQ.MD.WEB.Models;
using FQ.MD.WEB.ViewModels.Form;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FQ.MD.WEB.Services
{
    public class ModuleServices : BaseServices
    {
        private UnitOfWork _unitOfWork = new UnitOfWork();
        private LogErrorServices _errorLogServices = new LogErrorServices();

        public IEnumerable<GT_MASTER_MODULES> Get(
            Expression<Func<GT_MASTER_MODULES, bool>> filter = null,
            Func<IQueryable<GT_MASTER_MODULES>, IOrderedQueryable<GT_MASTER_MODULES>> orderBy = null,
            int? skip = null,
            int? take = null)
        {
            Expression<Func<GT_MASTER_MODULES, bool>> isNotDeleted = u => !u.IsDeleted;
            if (filter != null)
            {
                filter = filter.AndAlso(isNotDeleted);
            }
            else
            {
                filter = isNotDeleted;
            }

            return _unitOfWork.ModuleRepository.Get(filter: filter, orderBy: orderBy, skip: skip, take: take);
        }

        public GT_MASTER_MODULES GetModuleById(int Id)
        {
            Expression<Func<GT_MASTER_MODULES, bool>> filter = u => !u.IsDeleted && u.ModuleId == Id;
            return _unitOfWork.ModuleRepository.Get(filter: filter).FirstOrDefault();
        }

        public ServiceResult AddModule(ModuleAddForm form, GT_MASTER_USERS actionUser)
        {
            var res = new ServiceResult() { IsSuccess = false };
            try
            {
                var model = new GT_MASTER_MODULES();
                model.Module = form.Module.ToUpper();
                model.Note = form.Note;
                model.CreatedBy = actionUser.UserId;
                model.CreatedDate = DateTime.Now;

                _unitOfWork.ModuleRepository.Insert(model);
                _unitOfWork.Save();
                res.IsSuccess = true;
            }
            catch (Exception e)
            {
                var log = _errorLogServices.AddLog(e, actionUser.UserId);

                res.Message = log.Message;
            }
            return res;
        }

        public ServiceResult UpdateModule(ModuleEditForm form, GT_MASTER_USERS actionUser)
        {
            var res = new ServiceResult() { IsSuccess = false };

            var model = this.GetModuleById(form.ModuleId);
            if (model == null)
            {
                res.Message = "ValidationIdNotFound";
                return res;
            }

            try
            {
                //model.Module = form.Module;
                model.Note = form.Note;
                model.LastUpdatedBy = actionUser.UserId;
                model.LastUpdatedDate = DateTime.Now;

                _unitOfWork.ModuleRepository.Update(model);
                _unitOfWork.Save();
                res.IsSuccess = true;
            }
            catch (Exception e)
            {
                var log = _errorLogServices.AddLog(e, actionUser.UserId);

                res.Message = log.Message;
            }
            return res;
        }

        public ServiceResult DeleteModule(ModuleDeleteForm form, GT_MASTER_USERS actionUser)
        {
            var res = new ServiceResult() { IsSuccess = false };

            var model = this.GetModuleById(form.ModuleId);
            if (model == null)
            {
                res.Message = "ValidationIdNotFound";
                return res;
            }

            try
            {
                model.IsDeleted = true;
                model.DeletedBy = actionUser.UserId;
                model.DeletedDate = DateTime.Now;
                model.LastUpdatedBy = actionUser.UserId;
                model.LastUpdatedDate = DateTime.Now;

                _unitOfWork.ModuleRepository.Update(model);
                _unitOfWork.Save();
                res.IsSuccess = true;
            }
            catch (Exception e)
            {
                var log = _errorLogServices.AddLog(e, actionUser.UserId);

                res.Message = log.Message;
            }
            return res;
        }
    }
}
