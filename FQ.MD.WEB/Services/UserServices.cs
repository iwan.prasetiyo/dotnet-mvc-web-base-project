﻿using FQ.MD.WEB.ViewModels.Form;
using FQ.MD.WEB.ViewModels.JSON;
using FQ.MD.WEB.DAL;
using FQ.MD.WEB.Helpers;
using FQ.MD.WEB.Models;
using FQ.MD.WEB.Resources;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FQ.MD.WEB.Services
{
    public class UserServices : BaseServices
    {
        private UnitOfWork _unitOfWork = new UnitOfWork();
        private LogErrorServices _errorLogServices = new LogErrorServices();

        public IEnumerable<GT_MASTER_USERS> Get(
            Expression<Func<GT_MASTER_USERS, bool>> filter = null,
            Func<IQueryable<GT_MASTER_USERS>, IOrderedQueryable<GT_MASTER_USERS>> orderBy = null,
            int? skip = null,
            int? take = null)
        {
            Expression<Func<GT_MASTER_USERS, bool>> isNotDeleted = u => !u.IsDeleted;
            if (filter != null)
            {
                filter = filter.AndAlso(isNotDeleted);
            }
            else
            {
                filter = isNotDeleted;
            }

            return _unitOfWork.UserRepository.Get(filter: filter, orderBy: orderBy, skip: skip, take: take);
        }

        public GT_MASTER_USERS GetUserById(int Id)
        {
            Expression<Func<GT_MASTER_USERS, bool>> filter = u => !u.IsDeleted && u.UserId == Id;
            return _unitOfWork.UserRepository.Get(filter: filter).FirstOrDefault();
        }

        public GT_MASTER_USERS GetUserByEmailAddress(string EmailAddress)
        {
            Expression<Func<GT_MASTER_USERS, bool>> filter = u => !u.IsDeleted && u.Email == EmailAddress;
            return _unitOfWork.UserRepository.Get(filter: filter).FirstOrDefault();
        }

        public GT_MASTER_USERS GetUserByUsername(string Username)
        {
            Expression<Func<GT_MASTER_USERS, bool>> filter = u => !u.IsDeleted && u.Username.ToLower() == Username.ToLower();
            return _unitOfWork.UserRepository.Get(filter: filter).FirstOrDefault();
        }

        public ServiceResult AddUser(UserAddForm form, GT_MASTER_USERS actionUser)
        {
            var res = new ServiceResult() { IsSuccess = false };
            try
            {
                string EncPassword, salt;
                this.GeneratePassword(form.Password, out EncPassword, out salt);

                var model = new GT_MASTER_USERS();
                model.Username = form.Username.ToLower();
                model.Name = form.Name;
                model.Email = form.Email;
                model.Password = EncPassword;
                model.Salt = salt;
                model.GroupId = form.GroupId;
                model.IsActive = form.IsActive;
                model.CreatedBy = actionUser.UserId;
                model.CreatedDate = DateTime.Now;

                _unitOfWork.UserRepository.Insert(model);
                _unitOfWork.Save();

                res.IsSuccess = true;
            }
            catch (Exception e)
            {
                var log = _errorLogServices.AddLog(e, actionUser.UserId);
                res.Message = log.Message;
            }
            return res;
        }

        public ServiceResult UpdateUser(UserEditForm form, GT_MASTER_USERS actionUser)
        {
            var res = new ServiceResult() { IsSuccess = false };

            var model = this.GetUserById(form.UserId);
            if (model == null)
            {
                res.Message = "ValidationIdNotFound";
                return res;
            }

            try
            {
                //model.Username = form.Username.ToLower();
                model.Name = form.Name;
                model.Email = form.Email;
                model.GroupId = form.GroupId;
                model.IsActive = form.IsActive;
                if (!form.IsActive)
                {
                    model.InactivatedBy = actionUser.UserId;
                    model.InactivatedDate = DateTime.Now;
                }
                model.LastUpdatedBy = actionUser.UserId;
                model.LastUpdateDate = DateTime.Now;

                _unitOfWork.UserRepository.Update(model);
                _unitOfWork.Save();

                res.IsSuccess = true;
            }
            catch (Exception e)
            {
                var log = _errorLogServices.AddLog(e, actionUser.UserId);
                res.Message = log.Message;
            }
            return res;
        }

        public ServiceResult DeleteUser(UserDeleteForm form, GT_MASTER_USERS actionUser)
        {
            var res = new ServiceResult() { IsSuccess = false };

            var model = this.GetUserById(form.UserId);
            if (model == null)
            {
                res.Message = "ValidationIdNotFound";
                return res;
            }

            try
            {
                model.IsDeleted = true;
                model.DeletedBy = actionUser.UserId;
                model.DeletedDate = DateTime.Now;
                model.LastUpdatedBy = actionUser.UserId;
                model.LastUpdateDate = DateTime.Now;

                _unitOfWork.UserRepository.Update(model);
                _unitOfWork.Save();

                res.IsSuccess = true;
            }
            catch (Exception e)
            {
                var log = _errorLogServices.AddLog(e, actionUser.UserId);
                res.Message = log.Message;
            }
            return res;
        }

        public ServiceResultWithData<GT_MASTER_USERS> Login(string Username, string Password = null, int CultureId = 0)
        {
            var res = new ServiceResultWithData<GT_MASTER_USERS>() { IsSuccess = false };

            var user = this.Get(filter: q => q.Username.ToLower() == Username.ToLower() || q.Email.ToLower() == Username.ToLower()).FirstOrDefault();
            if (user == null)
            {
                res.Message = Res.ValidationNoUsersFound;
                return res;
            }

            var crypto = new SimpleCrypto.PBKDF2();
            if (user.Password == crypto.Compute(Password, user.Salt))
            {
                res.IsSuccess = true;
                res.Data = user;

                return res;
            }
            res.Message = Res.ValidationPasswordConfirm;
            return res;
        }

        public ServiceResult ChangeUserPassword(int UserId, string NewPassword, int UpdatedBy)
        {
            var action = new ServiceResult() { IsSuccess = false };
            action.IsSuccess = false;

            var model = GetUserById(UserId);
            if (model == null)
            {
                action.Message = Res.ValidationNoUsersFound;
                return action;
            }

            try
            {
                string EncPassword, salt;
                GeneratePassword(NewPassword, out EncPassword, out salt);
                model.Password = EncPassword;
                model.Salt = salt;
                model.LastUpdatedBy = UpdatedBy;
                model.LastUpdateDate = DateTime.Now;

                _unitOfWork.UserRepository.Update(model);
                _unitOfWork.Save();

                action.IsSuccess = true;
            }
            catch (Exception e)
            {
                var log = _errorLogServices.AddLog(e, UpdatedBy);
                action.Message = log.Message;
            }
            return action;
        }

        public ServiceResult ChangePassword(int UserId, string Password, string NewPassword)
        {
            var action = new ServiceResult();
            action.IsSuccess = false;

            var model = GetUserById(UserId);
            if (model == null)
            {
                action.Message = Res.ValidationNoUsersFound;
                return action;
            }

            var crypto = new SimpleCrypto.PBKDF2();
            if (model.Password != crypto.Compute(Password, model.Salt))
            {
                action.Message = Res.ValidationPasswordConfirm;
                return action;
            }

            string EncPassword, salt;
            GeneratePassword(NewPassword, out EncPassword, out salt);

            try
            {
                model.Password = EncPassword;
                model.Salt = salt;

                _unitOfWork.UserRepository.Update(model);
                _unitOfWork.Save();
                action.IsSuccess = true;
                action.Message = "Success";
                return action;
            }
            catch (Exception e)
            {
                var log = _errorLogServices.AddLog(e, model.CreatedBy);
                action.Message = log.Message;
                return action;
            }
        }

        public void GeneratePassword(string Password, out string EncriptedPassword, out string Salt)
        {
            PasswordHelpers.GeneratePassword(Password, out EncriptedPassword, out Salt);
        }
    }
}
