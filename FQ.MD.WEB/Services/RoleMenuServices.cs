﻿using FQ.MD.WEB.DAL;
using FQ.MD.WEB.Models;
using FQ.MD.WEB.ViewModels.Form;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FQ.MD.WEB.Services
{
    public class RoleMenuServices : BaseServices
    {
        private UnitOfWork _unitOfWork = new UnitOfWork();
        private LogErrorServices _errorLogServices = new LogErrorServices();

        public IEnumerable<GT_MASTER_ROLE_MENUS> Get(
            Expression<Func<GT_MASTER_ROLE_MENUS, bool>> filter = null,
            Func<IQueryable<GT_MASTER_ROLE_MENUS>, IOrderedQueryable<GT_MASTER_ROLE_MENUS>> orderBy = null,
            int? skip = null,
            int? take = null)
        {
            Expression<Func<GT_MASTER_ROLE_MENUS, bool>> isNotDeleted = u => !u.IsDeleted;
            if (filter != null)
            {
                filter = filter.AndAlso(isNotDeleted);
            }
            else
            {
                filter = isNotDeleted;
            }

            return _unitOfWork.RoleMenuRepository.Get(filter: filter, orderBy: orderBy, skip: skip, take: take);
        }

        public GT_MASTER_ROLE_MENUS GetRoleMenuById(int Id)
        {
            Expression<Func<GT_MASTER_ROLE_MENUS, bool>> filter = u => !u.IsDeleted && u.RoleMenuId == Id;
            return _unitOfWork.RoleMenuRepository.Get(filter: filter).FirstOrDefault();
        }

        public ServiceResult AddRoleMenu(RoleMenuAddForm form, GT_MASTER_USERS actionUser)
        {
            var res = new ServiceResult() { IsSuccess = false };
            try
            {
                var model = new GT_MASTER_ROLE_MENUS();
                model.RoleId = form.RoleId;
                model.MenuId = form.MenuId;
                model.GrantCreate = form.GrantCreate;
                model.GrantRead = form.GrantRead;
                model.GrantUpdate = form.GrantUpdate;
                model.GrantDelete = form.GrantDelete;
                model.GrantSync = form.GrantSync;
                model.GrantUpload = form.GrantUpload;

                model.IsActive = form.IsActive;
                model.CreatedBy = actionUser.UserId;
                model.CreatedDate = DateTime.Now;
                _unitOfWork.RoleMenuRepository.Insert(model);

                _unitOfWork.Save();
                res.IsSuccess = true;
            }
            catch (Exception e)
            {
                var log = _errorLogServices.AddLog(e, actionUser.UserId);
                res.Message = log.Message;
            }
            return res;
        }

        public ServiceResult UpdateRoleMenu(RoleMenuEditForm form, GT_MASTER_USERS actionUser)
        {
            var res = new ServiceResult() { IsSuccess = false };
            var model = this.GetRoleMenuById(form.RoleMenuId);
            if (model == null)
            {
                res.Message = "ValidationIdNotFound";
                return res;
            }

            try
            {
                //model.MenuId = form.MenuId;
                model.GrantCreate = form.GrantCreate;
                model.GrantDelete = form.GrantDelete;
                model.GrantRead = form.GrantRead;
                model.GrantUpdate = form.GrantUpdate;
                model.GrantSync = form.GrantSync;
                model.GrantUpload = form.GrantUpload;
                model.IsActive = form.IsActive;
                if (!form.IsActive)
                {
                    model.InactivatedBy = actionUser.UserId;
                    model.InactivatedDate = DateTime.Now;
                }
                model.LastUpdatedBy = actionUser.UserId;
                model.LastUpdatedDate = DateTime.Now;

                _unitOfWork.RoleMenuRepository.Update(model);
                _unitOfWork.Save();

                res.IsSuccess = true;
            }
            catch (Exception e)
            {
                var log = _errorLogServices.AddLog(e, actionUser.CreatedBy);
                res.Message = log.Message;
            }
            return res;
        }

        public ServiceResult DeleteRoleMenu(RoleMenuDeleteForm form, GT_MASTER_USERS actionUser)
        {
            var res = new ServiceResult() { IsSuccess = false };
            var model = this.GetRoleMenuById(form.RoleMenuId);
            if (model == null)
            {
                res.Message = "ValidationIdNotFound";
                return res;
            }

            try
            {
                model.IsDeleted = true;
                model.DeletedBy = actionUser.UserId;
                model.DeletedDate = DateTime.Now;
                model.LastUpdatedBy = actionUser.UserId;
                model.LastUpdatedDate = DateTime.Now;

                _unitOfWork.RoleMenuRepository.Update(model);
                _unitOfWork.Save();

                res.IsSuccess = true;
            }
            catch (Exception e)
            {
                var log = _errorLogServices.AddLog(e, actionUser.CreatedBy);
                res.Message = log.Message;
            }
            return res;
        }
    }
}
