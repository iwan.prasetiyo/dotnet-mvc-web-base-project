﻿using FQ.MD.WEB.DAL;
using FQ.MD.WEB.Helpers;
using FQ.MD.WEB.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FQ.MD.WEB.Services
{
    public class DatabaseSeederServices
    {
        private UnitOfWork _unitOfWork = new UnitOfWork();

        public void Seed()
        {
            var companyRepository = _unitOfWork.CompanyRepository;
            var groupRepository = _unitOfWork.GroupRepository;
            var userRepository = _unitOfWork.UserRepository;
            var roleRepository = _unitOfWork.RoleRepository;
            var userRoleRepository = _unitOfWork.UserRoleRepository;
            var menuRepository = _unitOfWork.MenuRepository;
            var roleMenuRepository = _unitOfWork.RoleMenuRepository;
            var moduleRepository = _unitOfWork.ModuleRepository;
            var statusRepository = _unitOfWork.StatusRepository;
            var logErrorRepository = _unitOfWork.LogErrorRepository;

            if (companyRepository.Get().Count() == 0 && groupRepository.Get().Count() == 0 && userRepository.Get().Count() == 0)
            {
                #region companies and groups seed
                var company = new GT_MASTER_COMPANIES()
                {
                    CompanyName = "DEV",
                    IsActive = true,
                    CreatedDate = DateTime.Now
                };
                companyRepository.Insert(company);

                var groupSysAdmin = new GT_MASTER_GROUPS()
                {
                    GroupName = "SYSTEM ADMINISTRATOR",
                    CompanyId = 1,
                    Note = "Group for users system administrator",
                    IsActive = true,
                    CreatedDate = DateTime.Now
                };
                groupRepository.Insert(groupSysAdmin);

                var groupAdmin = new GT_MASTER_GROUPS()
                {
                    GroupName = "ADMINISTRATOR",
                    CompanyId = 1,
                    Note = "Group for users administrator",
                    IsActive = true,
                    CreatedDate = DateTime.Now
                };
                groupRepository.Insert(groupAdmin);

                _unitOfWork.Save();
                #endregion

                #region users seed
                string sysAdminPass = "sysadmin";
                string sysAdminEncPass;
                string sysAdminSalt;
                PasswordHelpers.GeneratePassword(sysAdminPass, out sysAdminEncPass, out sysAdminSalt);
                var userSysAdmin = new GT_MASTER_USERS()
                {
                    Username = "sysadmin",
                    Name = "SYSTEM ADMINISTRATOR",
                    Email = "sysadmin@fq.md",
                    Password = sysAdminEncPass,
                    Salt = sysAdminSalt,
                    GroupId = 1,
                    IsActive = true,
                    CreatedDate = DateTime.Now
                };
                userRepository.Insert(userSysAdmin);

                string adminPass = "admin";
                string adminEncPass;
                string adminSalt;
                PasswordHelpers.GeneratePassword(adminPass, out adminEncPass, out adminSalt);
                var userAdmin = new GT_MASTER_USERS()
                {
                    Username = "admin",
                    Name = "ADMINISTRATOR",
                    Email = "admin@fq.md",
                    Password = adminEncPass,
                    Salt = adminSalt,
                    GroupId = 2,
                    IsActive = true,
                    CreatedBy = 1,
                    CreatedDate = DateTime.Now
                };
                userRepository.Insert(userAdmin);

                _unitOfWork.Save();
                #endregion

                #region roles and user admin roles seed
                var roleSysAdmin = new GT_MASTER_ROLES()
                {
                    Role = "SYSTEM ADMINISTRATOR",
                    Description = "Role for system administrator",
                    IsActive = true,
                    CreatedBy = 1,
                    CreatedDate = DateTime.Now
                };
                roleRepository.Insert(roleSysAdmin);

                var roleAdmin = new GT_MASTER_ROLES()
                {
                    Role = "ADMINISTRATOR",
                    Description = "Role for administrator",
                    IsActive = true,
                    CreatedBy = 1,
                    CreatedDate = DateTime.Now
                };
                roleRepository.Insert(roleAdmin);

                var userRoleSysAdmin = new GT_MASTER_USER_ROLES()
                {
                    UserId = 1,
                    RoleId = 1,
                    IsActive = true,
                    CreatedBy = 1,
                    CreatedDate = DateTime.Now
                };
                userRoleRepository.Insert(userRoleSysAdmin);

                var userRoleAdmin = new GT_MASTER_USER_ROLES()
                {
                    UserId = 2,
                    RoleId = 2,
                    IsActive = true,
                    CreatedBy = 1,
                    CreatedDate = DateTime.Now
                };
                userRoleRepository.Insert(userRoleAdmin);

                _unitOfWork.Save();
                #endregion

                #region menus seed
                var menuLogin = new GT_MASTER_MENUS()
                {
                    Key = "Login",
                    Description = "Menu dummy for login",
                    Icon = "fas fa-sign-in-alt",
                    Sequence = 0,
                    IsLink = false,
                    IsActive = false,
                    CreatedBy = 1,
                    CreatedDate = DateTime.Now
                };
                menuRepository.Insert(menuLogin);

                var menuLogout = new GT_MASTER_MENUS()
                {
                    Key = "Logout",
                    Description = "Menu dummy for logout",
                    Icon = "fas fa-sign-in-alt",
                    Sequence = 0,
                    IsLink = false,
                    IsActive = false,
                    CreatedBy = 1,
                    CreatedDate = DateTime.Now
                };
                menuRepository.Insert(menuLogout);

                var menuMaster = new GT_MASTER_MENUS()
                {
                    Key = "MasterData",
                    Description = "Parent menu for master data",
                    Icon = "fas fa-folder",
                    Sequence = 10,
                    IsLink = false,
                    IsActive = true,
                    CreatedBy = 1,
                    CreatedDate = DateTime.Now
                };
                menuRepository.Insert(menuMaster);

                var menuUserMgn = new GT_MASTER_MENUS()
                {
                    Key = "UserManagement",
                    Description = "Parent menu for user management",
                    Icon = "fas fa-user-shield",
                    Sequence = 20,
                    IsLink = false,
                    IsActive = true,
                    CreatedBy = 1,
                    CreatedDate = DateTime.Now
                };
                menuRepository.Insert(menuUserMgn);

                var menuCompanies = new GT_MASTER_MENUS()
                {
                    Key = "Companies",
                    Description = "Menu for data companies",
                    Icon = "fas fa-building",
                    Sequence = 10,
                    URL = "/Companies",
                    ParentId = 3,
                    IsLink = true,
                    IsActive = true,
                    CreatedBy = 1,
                    CreatedDate = DateTime.Now
                };
                menuRepository.Insert(menuCompanies);

                var menuGroups = new GT_MASTER_MENUS()
                {
                    Key = "Groups",
                    Description = "Menu for data user groups",
                    Icon = "fas fa-users",
                    Sequence = 10,
                    URL = "/Groups",
                    ParentId = 4,
                    IsLink = true,
                    IsActive = true,
                    CreatedBy = 1,
                    CreatedDate = DateTime.Now
                };
                menuRepository.Insert(menuGroups);

                var menuUsers = new GT_MASTER_MENUS()
                {
                    Key = "Users",
                    Description = "Menu for data users",
                    Icon = "fas fa-user",
                    Sequence = 30,
                    URL = "/Users",
                    ParentId = 4,
                    IsLink = true,
                    IsActive = true,
                    CreatedBy = 1,
                    CreatedDate = DateTime.Now
                };
                menuRepository.Insert(menuUsers);

                var menuMenu = new GT_MASTER_MENUS()
                {
                    Key = "Menus",
                    Description = "Menu for data menus",
                    Icon = "fas fa-bars",
                    Sequence = 20,
                    URL = "/Menus",
                    ParentId = 3,
                    IsLink = true,
                    IsActive = true,
                    CreatedBy = 1,
                    CreatedDate = DateTime.Now
                };
                menuRepository.Insert(menuMenu);

                var menuRole = new GT_MASTER_MENUS()
                {
                    Key = "Roles",
                    Description = "Menu for data roles",
                    Icon = "fas fa-user-tag",
                    Sequence = 20,
                    URL = "/Roles",
                    ParentId = 4,
                    IsLink = true,
                    IsActive = true,
                    CreatedBy = 1,
                    CreatedDate = DateTime.Now
                };
                menuRepository.Insert(menuRole);

                var menuModule = new GT_MASTER_MENUS()
                {
                    Key = "Modules",
                    Description = "Menu for data module",
                    Icon = "fas fa-file-code",
                    Sequence = 30,
                    URL = "/Modules",
                    ParentId = 3,
                    IsLink = true,
                    IsActive = true,
                    CreatedBy = 1,
                    CreatedDate = DateTime.Now
                };
                menuRepository.Insert(menuModule);

                _unitOfWork.Save();
                #endregion

                #region role menu sysadmin seed
                var roleSysAdminCompanies = new GT_MASTER_ROLE_MENUS()
                {
                    RoleId = 1,
                    MenuId = 5,
                    GrantCreate = false,
                    GrantRead = true,
                    GrantUpdate = false,
                    GrantDelete = false,
                    GrantSync = false,
                    GrantUpload = false,
                    IsActive = true,
                    CreatedBy = 1,
                    CreatedDate = DateTime.Now
                };
                roleMenuRepository.Insert(roleSysAdminCompanies);

                var roleSysAdminGroups = new GT_MASTER_ROLE_MENUS()
                {
                    RoleId = 1,
                    MenuId = 6,
                    GrantCreate = true,
                    GrantRead = true,
                    GrantUpdate = true,
                    GrantDelete = true,
                    GrantSync = false,
                    GrantUpload = false,
                    IsActive = true,
                    CreatedBy = 1,
                    CreatedDate = DateTime.Now
                };
                roleMenuRepository.Insert(roleSysAdminGroups);

                var roleSysAdminUsers = new GT_MASTER_ROLE_MENUS()
                {
                    RoleId = 1,
                    MenuId = 7,
                    GrantCreate = true,
                    GrantRead = true,
                    GrantUpdate = true,
                    GrantDelete = true,
                    GrantSync = false,
                    GrantUpload = false,
                    IsActive = true,
                    CreatedBy = 1,
                    CreatedDate = DateTime.Now
                };
                roleMenuRepository.Insert(roleSysAdminUsers);

                var roleSysAdminMenus = new GT_MASTER_ROLE_MENUS()
                {
                    RoleId = 1,
                    MenuId = 8,
                    GrantCreate = true,
                    GrantRead = true,
                    GrantUpdate = true,
                    GrantDelete = true,
                    GrantSync = false,
                    GrantUpload = false,
                    IsActive = true,
                    CreatedBy = 1,
                    CreatedDate = DateTime.Now
                };
                roleMenuRepository.Insert(roleSysAdminMenus);

                var roleSysAdminRoles = new GT_MASTER_ROLE_MENUS()
                {
                    RoleId = 1,
                    MenuId = 9,
                    GrantCreate = true,
                    GrantRead = true,
                    GrantUpdate = true,
                    GrantDelete = true,
                    GrantSync = false,
                    GrantUpload = false,
                    IsActive = true,
                    CreatedBy = 1,
                    CreatedDate = DateTime.Now
                };
                roleMenuRepository.Insert(roleSysAdminRoles);

                var roleSysAdminModule = new GT_MASTER_ROLE_MENUS()
                {
                    RoleId = 1,
                    MenuId = 10,
                    GrantCreate = true,
                    GrantRead = true,
                    GrantUpdate = true,
                    GrantDelete = true,
                    GrantSync = false,
                    GrantUpload = false,
                    IsActive = true,
                    CreatedBy = 1,
                    CreatedDate = DateTime.Now
                };
                roleMenuRepository.Insert(roleSysAdminModule);

                _unitOfWork.Save();
                #endregion

                #region role menu admin seed
                var roleAdminCompanies = new GT_MASTER_ROLE_MENUS()
                {
                    RoleId = 2,
                    MenuId = 5,
                    GrantCreate = false,
                    GrantRead = true,
                    GrantUpdate = false,
                    GrantDelete = false,
                    GrantSync = false,
                    GrantUpload = false,
                    IsActive = true,
                    CreatedBy = 1,
                    CreatedDate = DateTime.Now
                };
                roleMenuRepository.Insert(roleAdminCompanies);

                var roleAdminGroups = new GT_MASTER_ROLE_MENUS()
                {
                    RoleId = 2,
                    MenuId = 6,
                    GrantCreate = true,
                    GrantRead = true,
                    GrantUpdate = true,
                    GrantDelete = false,
                    GrantSync = false,
                    GrantUpload = false,
                    IsActive = true,
                    CreatedBy = 1,
                    CreatedDate = DateTime.Now
                };
                roleMenuRepository.Insert(roleAdminGroups);

                var roleAdminUsers = new GT_MASTER_ROLE_MENUS()
                {
                    RoleId = 2,
                    MenuId = 7,
                    GrantCreate = true,
                    GrantRead = true,
                    GrantUpdate = true,
                    GrantDelete = false,
                    GrantSync = false,
                    GrantUpload = false,
                    IsActive = true,
                    CreatedBy = 1,
                    CreatedDate = DateTime.Now
                };
                roleMenuRepository.Insert(roleAdminUsers);

                var roleAdminRoles = new GT_MASTER_ROLE_MENUS()
                {
                    RoleId = 2,
                    MenuId = 9,
                    GrantCreate = false,
                    GrantRead = true,
                    GrantUpdate = false,
                    GrantDelete = false,
                    GrantSync = false,
                    GrantUpload = false,
                    IsActive = true,
                    CreatedBy = 1,
                    CreatedDate = DateTime.Now
                };
                roleMenuRepository.Insert(roleAdminRoles);

                var roleAdminModule = new GT_MASTER_ROLE_MENUS()
                {
                    RoleId = 2,
                    MenuId = 10,
                    GrantCreate = false,
                    GrantRead = true,
                    GrantUpdate = false,
                    GrantDelete = false,
                    GrantSync = false,
                    GrantUpload = false,
                    IsActive = true,
                    CreatedBy = 1,
                    CreatedDate = DateTime.Now
                };
                roleMenuRepository.Insert(roleAdminModule);

                _unitOfWork.Save();
                #endregion

                #region log error seed
                var logErrorDuplicate = new GT_LOG_APP_ERRORS()
                {
                    ErrorCode = -2146232060,
                    DisplayKey = "ServerErrorDuplicateData",
                    CreatedBy = 1,
                    CreatedDate = DateTime.Now
                };
                logErrorRepository.Insert(logErrorDuplicate);

                var logEFValidate = new GT_LOG_APP_ERRORS()
                {
                    ErrorCode = -2146232032,
                    DisplayKey = "ServerErrorEFValidationExp",
                    CreatedBy = 1,
                    CreatedDate = DateTime.Now
                };
                logErrorRepository.Insert(logEFValidate);

                var logHttpFailed = new GT_LOG_APP_ERRORS()
                {
                    ErrorCode = -2147467259,
                    DisplayKey = "HttpRequestError",
                    CreatedBy = 1,
                    CreatedDate = DateTime.Now
                };
                logErrorRepository.Insert(logHttpFailed);

                _unitOfWork.Save();
                #endregion
            }
        }
    }
}
