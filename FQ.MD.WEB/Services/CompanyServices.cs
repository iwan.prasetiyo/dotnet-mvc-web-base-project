﻿using FQ.MD.WEB.DAL;
using FQ.MD.WEB.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
namespace FQ.MD.WEB.Services
{
    public class CompanyServices : BaseServices
    {
        private UnitOfWork _unitOfWork = new UnitOfWork();
        private UserServices _userServices = new UserServices();
        private LogErrorServices _errorLogServices = new LogErrorServices();

        public IEnumerable<GT_MASTER_COMPANIES> Get(
            Expression<Func<GT_MASTER_COMPANIES, bool>> filter = null,
            Func<IQueryable<GT_MASTER_COMPANIES>, IOrderedQueryable<GT_MASTER_COMPANIES>> orderBy = null,
            int? take = null,
            int? start = null
            )
        {
            Expression<Func<GT_MASTER_COMPANIES, bool>> isNotDeleted = u => !u.IsDeleted;
            if (filter != null)
            {
                filter = filter.AndAlso(isNotDeleted);
            }
            else
            {
                filter = isNotDeleted;
            }

            return _unitOfWork.CompanyRepository.Get(filter: filter, orderBy: orderBy, skip: start, take: take);
        }

        public GT_MASTER_COMPANIES GetCompanyById(int Id)
        {
            Expression<Func<GT_MASTER_COMPANIES, bool>> filter = u => !u.IsDeleted && u.CompanyId == Id;
            return _unitOfWork.CompanyRepository.Get(filter: filter).FirstOrDefault();
        }
    }
}
