﻿using FQ.MD.WEB.DAL;
using FQ.MD.WEB.Models;
using FQ.MD.WEB.ViewModels.Form;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FQ.MD.WEB.Services
{
    public class RoleServices : BaseServices
    {
        private UnitOfWork _unitOfWork = new UnitOfWork();
        private LogErrorServices _errorLogServices = new LogErrorServices();

        public IEnumerable<GT_MASTER_ROLES> Get(
            Expression<Func<GT_MASTER_ROLES, bool>> filter = null,
            Func<IQueryable<GT_MASTER_ROLES>, IOrderedQueryable<GT_MASTER_ROLES>> orderBy = null,
            int? skip = null,
            int? take = null)
        {
            Expression<Func<GT_MASTER_ROLES, bool>> isNotDeleted = u => !u.IsDeleted;
            if (filter != null)
            {
                filter = filter.AndAlso(isNotDeleted);
            }
            else
            {
                filter = isNotDeleted;
            }

            return _unitOfWork.RoleRepository.Get(filter: filter, orderBy: orderBy, skip: skip, take: take);
        }

        public GT_MASTER_ROLES GetRoleById(int Id)
        {
            Expression<Func<GT_MASTER_ROLES, bool>> filter = u => !u.IsDeleted && u.RoleId == Id;
            return _unitOfWork.RoleRepository.Get(filter: filter).FirstOrDefault();
        }

        public ServiceResult AddRole(RoleAddForm form, GT_MASTER_USERS actionUser)
        {
            var res = new ServiceResult() { IsSuccess = false };
            try
            {
                var model = new GT_MASTER_ROLES();
                model.Role = form.Role.ToUpper();
                model.Description = form.Description;
                model.IsActive = form.IsActive;
                model.CreatedBy = actionUser.UserId;
                model.CreatedDate = DateTime.Now;

                _unitOfWork.RoleRepository.Insert(model);
                _unitOfWork.Save();

                res.IsSuccess = true;
            }
            catch (Exception e)
            {
                var log = _errorLogServices.AddLog(e, actionUser.UserId);

                res.Message = log.Message;
            }
            return res;
        }

        public ServiceResult UpdateRole(RoleEditForm form, GT_MASTER_USERS actionUser)
        {
            var res = new ServiceResult() { IsSuccess = false };

            var model = this.GetRoleById(form.RoleId);
            if (model == null)
            {
                res.Message = "ValidationIdNotFound";
                return res;
            }

            try
            {
                //model.Role = form.Role;
                model.Description = form.Description;
                model.IsActive = form.IsActive;
                if (!form.IsActive)
                {
                    model.InactivatedBy = actionUser.UserId;
                    model.InactivatedDate = DateTime.Now;
                }
                model.LastUpdatedBy = actionUser.UserId;
                model.LastUpdatedDate = DateTime.Now;

                _unitOfWork.RoleRepository.Update(model);
                _unitOfWork.Save();

                res.IsSuccess = true;
            }
            catch (Exception e)
            {
                var log = _errorLogServices.AddLog(e, actionUser.UserId);
                res.Message = log.Message;
            }
            return res;
        }

        public ServiceResult DeleteRole(RoleDeleteForm form, GT_MASTER_USERS actionUser)
        {
            var res = new ServiceResult() { IsSuccess = false };

            var model = this.GetRoleById(form.RoleId);
            if (model == null)
            {
                res.Message = "ValidationIdNotFound";
                return res;
            }

            try
            {
                model.IsDeleted = true;
                model.DeletedBy = actionUser.UserId;
                model.DeletedDate = DateTime.Now;
                model.LastUpdatedBy = actionUser.UserId;
                model.LastUpdatedDate = DateTime.Now;

                _unitOfWork.RoleRepository.Update(model);
                _unitOfWork.Save();

                res.IsSuccess = true;
            }
            catch (Exception e)
            {
                var log = _errorLogServices.AddLog(e, actionUser.UserId);
                res.Message = log.Message;
            }
            return res;
        }
    }
}
