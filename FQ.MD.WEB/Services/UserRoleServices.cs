﻿using FQ.MD.WEB.DAL;
using FQ.MD.WEB.Models;
using FQ.MD.WEB.ViewModels.Form;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FQ.MD.WEB.Services
{
    public class UserRoleServices : BaseServices
    {
        private UnitOfWork _unitOfWork = new UnitOfWork();
        private LogErrorServices _errorLogServices = new LogErrorServices();

        public IEnumerable<GT_MASTER_USER_ROLES> GetRoleByUserId(int UserId)
        {
            Expression<Func<GT_MASTER_USER_ROLES, bool>> filter = u => !u.IsDeleted && u.UserId == UserId;
            return _unitOfWork.UserRoleRepository.Get(filter: filter);
        }

        public GT_MASTER_USER_ROLES GetUserRoleById(int Id)
        {
            Expression<Func<GT_MASTER_USER_ROLES, bool>> filter = u => !u.IsDeleted && u.UserRoleId == Id;
            return _unitOfWork.UserRoleRepository.Get(filter: filter).FirstOrDefault();
        }

        public ServiceResult AddUserRole(UserRoleAddForm form, GT_MASTER_USERS actionUser)
        {
            var res = new ServiceResult() { IsSuccess = false };
            try
            {
                var model = new GT_MASTER_USER_ROLES();
                model.UserId = form.UserId;
                model.RoleId = form.RoleId;
                model.IsActive = form.IsActive;
                model.CreatedBy = actionUser.UserId;
                model.CreatedDate = DateTime.Now;

                _unitOfWork.UserRoleRepository.Insert(model);
                _unitOfWork.Save();

                res.IsSuccess = true;
            }
            catch (Exception e)
            {
                var log = _errorLogServices.AddLog(e, actionUser.UserId);
                res.IsSuccess = false;
                res.Message = log.Message;
            }
            return res;
        }

        public ServiceResult UpdateUserRole(UserRoleEditForm form, GT_MASTER_USERS actionUser)
        {
            var res = new ServiceResult() { IsSuccess = false };

            var model = this.GetUserRoleById(form.UserRoleId);
            if (model == null)
            {
                res.Message = "ValidationIdNotFound";
                return res;
            }

            try
            {
                //model.UserId = form.UserId;
                //model.RoleId = form.RoleId;
                model.IsActive = form.IsActive;
                if (!form.IsActive)
                {
                    model.InactivatedBy = actionUser.UserId;
                    model.InactivatedDate = DateTime.Now;
                }
                model.LastUpdatedBy = actionUser.UserId;
                model.LastUpdatedDate = DateTime.Now;

                _unitOfWork.UserRoleRepository.Update(model);
                _unitOfWork.Save();

                res.IsSuccess = true;
            }
            catch (Exception e)
            {
                var log = _errorLogServices.AddLog(e, actionUser.UserId);
                res.Message = log.Message;
            }
            return res;
        }

        public ServiceResult DeleteUserRole(UserRoleDeleteForm form, GT_MASTER_USERS actionUser)
        {
            var res = new ServiceResult() { IsSuccess = false };

            var model = this.GetUserRoleById(form.UserRoleId);
            if (model == null)
            {
                res.Message = "ValidationIdNotFound";
                return res;
            }

            try
            {
                model.IsDeleted = true;
                model.DeletedBy = actionUser.UserId;
                model.DeletedDate = DateTime.Now;
                model.LastUpdatedBy = actionUser.UserId;
                model.LastUpdatedDate = DateTime.Now;

                _unitOfWork.UserRoleRepository.Update(model);
                _unitOfWork.Save();

                res.IsSuccess = true;
            }
            catch (Exception e)
            {
                var log = _errorLogServices.AddLog(e, actionUser.UserId);
                res.Message = log.Message;
            }
            return res;
        }
    }
}
