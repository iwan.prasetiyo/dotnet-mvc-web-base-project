﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FQ.MD.WEB.ViewModels
{
    public class HomeChatrsViewModel
    {
        public string[] Label { get; set; }
        public int[] Count { get; set; }
    }

    public class HomeChartDataViewModel
    {
        public string Date { get; set; }
        public int Count { get; set; }
    }
}