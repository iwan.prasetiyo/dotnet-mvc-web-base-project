﻿using FQ.MD.WEB.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FQ.MD.WEB.ViewModels
{
    public class ModuleViewModel
    {
        public int ModuleId { get; set; }
        public string Module { get; set; }
        public string Note { get; set; }

        public string Active { get; set; }
        public string Actions { get; set; }
        public List<StatusViewModel> Status { get; set; }
    }

    public class StatusViewModel
    {
        public int StatusId { get; set; }
        public string Status { get; set; }
        public string Description { get; set; }

        public string Actions { get; set; }
    }
}