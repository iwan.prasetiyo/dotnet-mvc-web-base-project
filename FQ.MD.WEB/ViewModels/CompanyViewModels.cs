﻿using FQ.MD.WEB.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FQ.MD.WEB.ViewModels
{
    public class CompanyViewModel
    {
        public int CompanyId { get; set; }

        [Required(ErrorMessageResourceType = typeof(Res), ErrorMessageResourceName = "ValidationRequired")]
        public string CompanyName { get; set; }
        [DataType(DataType.PhoneNumber)]
        public string CompanyPhone { get; set; }
        [DataType(DataType.EmailAddress)]
        public string CompanyEmail { get; set; }
        public string CompanyWebsite { get; set; }
        public bool IsActive { get; set; }
        public string Remark { get; set; }

        public string Active { get; set; }
        public string Actions { get; set; }
    }
}