﻿using FQ.MD.WEB.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FQ.MD.WEB.ViewModels
{
    public class UserViewModel
    {
        public int UserId { get; set; }
        public string Username { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string NIP { get; set; }
        public string GroupName { get; set; }
        public string Active { get; set; }
        public string Domain { get; set; }
        public string Actions { get; set; }
    }

    public class UserAddressViewModel
    {
        public int UserId { get; set; }
        public int UserAddressId { get; set; }
        public string Name { get; set; }
        public string NIP { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Active { get; set; }
        public string Actions { get; set; }
    }

    public class UserRoleViewModel
    {
        public int UserRoleId { get; set; }
        public string Role { get; set; }

        public string Active { get; set; }
        public string Actions { get; set; }
    }

    public class UserRoleIndexViewModel
    {
        public int UserId { get; set; }
        public string Name { get; set; }
        public string Group { get; set; }

        public List<UserRoleViewModel> Roles { get; set; }
    }
}