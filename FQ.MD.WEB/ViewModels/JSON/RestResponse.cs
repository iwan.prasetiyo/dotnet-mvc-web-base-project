﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FQ.MD.WEB.ViewModels.JSON
{
    public class RestResponse<TModel> where TModel : class
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public TModel Data { get; set; }
    }

    public class RestResponse
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }
}
