﻿using FQ.MD.WEB.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FQ.MD.WEB.ViewModels
{
    public class GroupViewModel
    {
        public int GroupId { get; set; }
        public string GroupName { get; set; }
        public string Company { get; set; }
        public string Parent { get; set; }
        public string Note { get; set; }

        public string Active { get; set; }
        public string Actions { get; set; }
    }
}