﻿using FQ.MD.WEB.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FQ.MD.WEB.ViewModels
{
    public class MenuViewModel
    {
        public int MenuId { get; set; }
        public string Key { get; set; }
        public string Description { get; set; }
        public string Url { get; set; }
        public string Icon { get; set; }
        public int Sequence { get; set; }
        public int? ParentId { get; set; }
        public string Parent { get; set; }
        public string IsLink { get; set; }
        public string Remark { get; set; }

        public string Active { get; set; }
        public string Actions { get; set; }

    }
}