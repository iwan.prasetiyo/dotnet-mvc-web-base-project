﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FQ.MD.WEB.ViewModels
{
    public class SelectItemIntValue
    {
        public int? Value { get; set; }
        public string Text { get; set; }
    }

    public class SelectItemStringValue
    {
        public string Value { get; set; }
        public string Text { get; set; }
    }

    public class APIResponse<TModel> where TModel : class
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public TModel Data { get; set; }
        public string[] Errors { get; set; }
    }

    public class APIResponse
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public string[] Errors { get; set; }
    }

    public class MenuItem
    {
        public int MenuId { get; set; }
        public int? ParentMenuId { get; set; }
        public string Url { get; set; }
        public string Menu { get; set; }
        public string Display { get; set; }
        public int Sequence { get; set; }
        public bool IsActive { get; set; }
    }

    public class ActionsSubmenu
    {
        public string SubmenuLabel { get; set; }
        public string SubmenuController { get; set; }
        public string SubmenuKey { get; set; }
    }

    public class AuthorizationResult
    {
        public bool IsAthorized { get; set; }
        public int MenuId { get; set; }
        public bool IsCreateGranted { get; set; }
        public bool IsReadGranted { get; set; }
        public bool IsUpdateGranted { get; set; }
        public bool IsDeleteGranted { get; set; }
        public bool IsSyncGranted { get; set; }
        public bool IsUploadGranted { get; set; }
    }
}