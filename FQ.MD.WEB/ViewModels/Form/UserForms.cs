﻿using FQ.MD.WEB.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FQ.MD.WEB.ViewModels.Form
{
    public class UserAddForm
    {
        [Required(ErrorMessageResourceType = typeof(Res), ErrorMessageResourceName = "ValidationRequired")]
        public string NIP { get; set; }
        [RegularExpression(@"^[a-zA-Z.]*$", ErrorMessageResourceType = typeof(Res), ErrorMessageResourceName = "ValidationUsernameFormat")]
        [Required(ErrorMessageResourceType = typeof(Res), ErrorMessageResourceName = "ValidationRequired")]
        public string Username { get; set; }
        [Required(ErrorMessageResourceType = typeof(Res), ErrorMessageResourceName = "ValidationRequired")]
        public string Name { get; set; }
        [Required(ErrorMessageResourceType = typeof(Res), ErrorMessageResourceName = "ValidationRequired")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [Required(ErrorMessageResourceType = typeof(Res), ErrorMessageResourceName = "ValidationRequired")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessageResourceType = typeof(Res), ErrorMessageResourceName = "ValidationPasswordConfirm")]
        public string ConfirmPassword { get; set; }
        [Required(ErrorMessageResourceType = typeof(Res), ErrorMessageResourceName = "ValidationRequired")]
        public int GroupId { get; set; }
        public bool IsActive { get; set; }
    }

    public class UserEditForm
    {
        public int UserId { get; set; }
        [Required(ErrorMessageResourceType = typeof(Res), ErrorMessageResourceName = "ValidationRequired")]
        public string NIP { get; set; }
        [Required(ErrorMessageResourceType = typeof(Res), ErrorMessageResourceName = "ValidationRequired")]
        public string Username { get; set; }
        [Required(ErrorMessageResourceType = typeof(Res), ErrorMessageResourceName = "ValidationRequired")]
        public string Name { get; set; }
        [Required(ErrorMessageResourceType = typeof(Res), ErrorMessageResourceName = "ValidationRequired")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        public int GroupId { get; set; }
        public bool IsActive { get; set; }
        public bool IsVerified { get; set; }
    }

    public class UserDeleteForm
    {
        public int UserId { get; set; }
        public string NIP { get; set; }
        public string Username { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public int GroupId { get; set; }
        public bool IsVerified { get; set; }
        [Required(ErrorMessageResourceType = typeof(Res), ErrorMessageResourceName = "ValidationRequired")]
        public string Remark { get; set; }
    }

    public class UserChangePasswordForm
    {
        [Required(ErrorMessageResourceType = typeof(Res), ErrorMessageResourceName = "ValidationRequired")]
        public int UserId { get; set; }

        [Required(ErrorMessageResourceType = typeof(Res), ErrorMessageResourceName = "ValidationRequired")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessageResourceType = typeof(Res), ErrorMessageResourceName = "ValidationPasswordConfirm")]
        public string ConfirmPassword { get; set; }
    }
}