﻿using FQ.MD.WEB.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FQ.MD.WEB.ViewModels.Form
{
    public class RoleMenuAddForm
    {
        [Required(ErrorMessageResourceType = typeof(Res), ErrorMessageResourceName = "ValidationRequired")]
        public int RoleId { get; set; }
        [Required(ErrorMessageResourceType = typeof(Res), ErrorMessageResourceName = "ValidationRequired")]
        public int MenuId { get; set; }
        public bool GrantCreate { get; set; }
        public bool GrantRead { get; set; }
        public bool GrantUpdate { get; set; }
        public bool GrantDelete { get; set; }
        public bool GrantSync { get; set; }
        public bool GrantUpload { get; set; }
        public bool IsActive { get; set; }
    }

    public class RoleMenuEditForm
    {
        public int RoleMenuId { get; set; }
        [Required(ErrorMessageResourceType = typeof(Res), ErrorMessageResourceName = "ValidationRequired")]
        public int RoleId { get; set; }
        [Required(ErrorMessageResourceType = typeof(Res), ErrorMessageResourceName = "ValidationRequired")]
        public int MenuId { get; set; }
        public bool GrantCreate { get; set; }
        public bool GrantRead { get; set; }
        public bool GrantUpdate { get; set; }
        public bool GrantDelete { get; set; }
        public bool GrantSync { get; set; }
        public bool GrantUpload { get; set; }
        public bool IsActive { get; set; }
    }

    public class RoleMenuDeleteForm
    {
        public int RoleMenuId { get; set; }
        [Required(ErrorMessageResourceType = typeof(Res), ErrorMessageResourceName = "ValidationRequired")]
        public int RoleId { get; set; }
        public int MenuId { get; set; }
        public bool GrantCreate { get; set; }
        public bool GrantRead { get; set; }
        public bool GrantUpdate { get; set; }
        public bool GrantDelete { get; set; }
        public bool GrantSync { get; set; }
        public bool GrantUpload { get; set; }
        public bool IsActive { get; set; }
        [Required(ErrorMessageResourceType = typeof(Res), ErrorMessageResourceName = "ValidationRequired")]
        public string Remark { get; set; }
    }
}