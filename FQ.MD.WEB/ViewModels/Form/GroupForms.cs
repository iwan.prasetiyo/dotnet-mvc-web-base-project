﻿using FQ.MD.WEB.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FQ.MD.WEB.ViewModels.Form
{
    public class GroupAddForm
    {
        [Required(ErrorMessageResourceType = typeof(Res), ErrorMessageResourceName = "ValidationRequired")]
        public string GroupName { get; set; }
        [Required(ErrorMessageResourceType = typeof(Res), ErrorMessageResourceName = "ValidationRequired")]
        public int CompanyId { get; set; }
        public int? ParentId { get; set; }
        public string Note { get; set; }
        public bool IsActive { get; set; }
    }

    public class GroupEditForm
    {
        public int GroupId { get; set; }
        [Required(ErrorMessageResourceType = typeof(Res), ErrorMessageResourceName = "ValidationRequired")]
        public string GroupName { get; set; }
        public int? ParentId { get; set; }
        public int CompanyId { get; set; }
        public string Note { get; set; }
        public bool IsActive { get; set; }
    }

    public class GroupDeleteForm
    {
        public int GroupId { get; set; }
        public string GroupName { get; set; }
        public int CompanyId { get; set; }
        public int? ParentId { get; set; }
        public string Note { get; set; }
        public bool IsActive { get; set; }
        [Required(ErrorMessageResourceType = typeof(Res), ErrorMessageResourceName = "ValidationRequired")]
        public string Remark { get; set; }
    }
}