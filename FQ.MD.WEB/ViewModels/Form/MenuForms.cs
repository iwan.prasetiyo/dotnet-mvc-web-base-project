﻿using FQ.MD.WEB.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FQ.MD.WEB.ViewModels.Form
{
    public class MenuAddForm
    {
        [Required(ErrorMessageResourceType = typeof(Res), ErrorMessageResourceName = "ValidationRequired")]
        public string Key { get; set; }
        [Required(ErrorMessageResourceType = typeof(Res), ErrorMessageResourceName = "ValidationRequired")]
        public string Description { get; set; }
        public string Url { get; set; }
        [Required(ErrorMessageResourceType = typeof(Res), ErrorMessageResourceName = "ValidationRequired")]
        public string Icon { get; set; }
        [Required(ErrorMessageResourceType = typeof(Res), ErrorMessageResourceName = "ValidationRequired")]
        public int Sequence { get; set; }
        public int? ParentId { get; set; }
        public bool IsActive { get; set; }
    }

    public class MenuEditForm
    {
        public int MenuId { get; set; }
        [Required(ErrorMessageResourceType = typeof(Res), ErrorMessageResourceName = "ValidationRequired")]
        public string Key { get; set; }
        [Required(ErrorMessageResourceType = typeof(Res), ErrorMessageResourceName = "ValidationRequired")]
        public string Description { get; set; }
        public string Url { get; set; }
        [Required(ErrorMessageResourceType = typeof(Res), ErrorMessageResourceName = "ValidationRequired")]
        public string Icon { get; set; }
        [Required(ErrorMessageResourceType = typeof(Res), ErrorMessageResourceName = "ValidationRequired")]
        public int Sequence { get; set; }
        public int? ParentId { get; set; }
        public bool IsActive { get; set; }
    }

    public class MenuDeleteForm
    {
        public int MenuId { get; set; }
        public string Key { get; set; }
        public string Description { get; set; }
        public string Url { get; set; }
        public string Icon { get; set; }
        public int Sequence { get; set; }
        public int? ParentId { get; set; }
        public bool IsActive { get; set; }
        [Required(ErrorMessageResourceType = typeof(Res), ErrorMessageResourceName = "ValidationRequired")]
        public string Remark { get; set; }
    }
}