﻿using FQ.MD.WEB.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FQ.MD.WEB.ViewModels.Form
{
    public class RoleAddForm
    {
        [Required(ErrorMessageResourceType = typeof(Res), ErrorMessageResourceName = "ValidationRequired")]
        public string Role { get; set; }
        [Required(ErrorMessageResourceType = typeof(Res), ErrorMessageResourceName = "ValidationRequired")]
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }

    public class RoleEditForm
    {
        public int RoleId { get; set; }
        [Required(ErrorMessageResourceType = typeof(Res), ErrorMessageResourceName = "ValidationRequired")]
        public string Role { get; set; }
        [Required(ErrorMessageResourceType = typeof(Res), ErrorMessageResourceName = "ValidationRequired")]
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }

    public class RoleDeleteForm
    {
        public int RoleId { get; set; }
        public string Role { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        [Required(ErrorMessageResourceType = typeof(Res), ErrorMessageResourceName = "ValidationRequired")]
        public string Remark { get; set; }
    }
}