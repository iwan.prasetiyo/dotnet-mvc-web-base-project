﻿using FQ.MD.WEB.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FQ.MD.WEB.ViewModels
{
    public class RoleViewModel
    {
        public int RoleId { get; set; }
        public string Role { get; set; }
        public string Description { get; set; }

        public string Active { get; set; }
        public string Actions { get; set; }

    }

    public class RoleMenuViewModel
    {
        public int Id { get; set; }
        public int RoleId { get; set; }
        public string Role { get; set; }
        public string Description { get; set; }
        public int MenuId { get; set; }
        public int? ParentMenuId { get; set; }
        public string Menu { get; set; }
        public bool IsLink { get; set; }
        public int Sequence { get; set; }
        public string Display { get; set; }

        public string Create { get; set; }
        public string Read { get; set; }
        public string Update { get; set; }
        public string Delete { get; set; }
        public string Sync { get; set; }
        public string Upload { get; set; }
        public string Active { get; set; }
        public string Actions { get; set; }
    }

    public class RoleMenuIndexViewModel
    {
        public int RoleId { get; set; }
        public string Role { get; set; }
        public string Description { get; set; }
        public List<RoleMenuViewModel> Menus { get; set; }
    }
}