﻿using FQ.MD.WEB.ViewModels.JSON;
using FQ.MD.WEB.Helpers;
using FQ.MD.WEB.Models;
using FQ.MD.WEB.Resources;
using FQ.MD.WEB.Services;
using FQ.MD.WEB.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using FQ.MD.WEB.ViewModels.Form;

namespace FQ.MD.WEB.Controllers
{
    [Authorize]
    public class RolesController : BaseController
    {
        private RoleServices _roleServices = new RoleServices();

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Index(DataTables model)
        {
            var searchKeywords = !String.IsNullOrEmpty(model.search.value) ? model.search.value.ToLower() : "";

            string sortBy = "Role";
            bool sortDir = true;

            if (model.order != null)
            {
                sortBy = model.columns[model.order[0].column].data;
                sortDir = model.order[0].dir.ToLower() == "asc";
            }

            Expression<Func<GT_MASTER_ROLES, bool>> filter = u => u.Role.ToLower().Contains(searchKeywords) || u.Description.Contains(searchKeywords);
            Func<IQueryable<GT_MASTER_ROLES>, IOrderedQueryable<GT_MASTER_ROLES>> orderBy = q => sortDir ? q.OrderBy(u => u.Role) : q.OrderByDescending(u => u.Role);

            if (sortBy == "Active")
            {
                orderBy = q => sortDir ? q.OrderBy(u => u.IsActive) : q.OrderByDescending(u => u.IsActive);
            }
            if (sortBy == "Description")
            {
                orderBy = q => sortDir ? q.OrderBy(u => u.Description) : q.OrderByDescending(u => u.Description);
            }

            var submenus = new List<ActionsSubmenu>() { new ActionsSubmenu() { SubmenuLabel = "Menus", SubmenuKey = "RoleId", SubmenuController = "RolesMenus" } };

            var models = _roleServices.Get(filter: filter, orderBy: orderBy, skip: model.start, take: model.length)
                .Select(c => new RoleViewModel
                {
                    Role = c.Role,
                    Description = c.Description,
                    Active = DisplayActiveIcon(c.IsActive),
                    Actions = DisplayActions(UserAuthorization, c.RoleId, submenus)
                });

            var totalResultsCount = _roleServices.Get().Count();
            var recordsFiltered = String.IsNullOrEmpty(model.search.value) ? totalResultsCount : _roleServices.Get(filter: filter).Count();

            return Json(new
            {
                draw = model.draw,
                recordsTotal = totalResultsCount,
                recordsFiltered = recordsFiltered,
                data = models.ToList()
            });
        }

        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Add(RoleAddForm form)
        {
            if (ModelState.IsValid)
            {
                var action = _roleServices.AddRole(form, UserAuthorized);
                if (action.IsSuccess)
                {
                    return RedirectToAction("Index");
                }

                var message = DisplayMessage(action.Message, new string[] { "Role" });
                ModelState.AddModelError("", message);
            }

            return View(form);
        }

        public ActionResult Edit(int Id)
        {
            var model = _roleServices.GetRoleById(Id);
            if (model == null)
            {
                return RedirectToAction("Index", "NotFound");
            }

            var form = new RoleEditForm();
            form.RoleId = model.RoleId;
            form.Role = model.Role;
            form.Description = model.Description;
            form.IsActive = model.IsActive;

            return View(form);
        }

        [HttpPost]
        public ActionResult Edit(RoleEditForm form)
        {
            if (ModelState.IsValid)
            {
                var action = _roleServices.UpdateRole(form, UserAuthorized);
                if (action.IsSuccess)
                {
                    return RedirectToAction("Index");
                }
            }

            return View(form);
        }

        public ActionResult Detail(int Id)
        {
            var model = _roleServices.GetRoleById(Id);
            if (model == null)
            {
                return RedirectToAction("Index", "NotFound");
            }

            var viewModel = new RoleViewModel();
            viewModel.RoleId = model.RoleId;
            viewModel.Role = model.Role;
            viewModel.Description = model.Description;
            viewModel.Active = DisplayActiveIcon(model.IsActive);

            return View(viewModel);
        }

        public ActionResult Delete(int Id)
        {
            var model = _roleServices.GetRoleById(Id);
            if (model == null)
            {
                return RedirectToAction("Index", "NotFound");
            }

            var viewModel = new RoleDeleteForm();
            viewModel.RoleId = model.RoleId;
            viewModel.Role = model.Role;
            viewModel.Description = model.Description;
            viewModel.IsActive = model.IsActive;

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Delete(RoleDeleteForm form)
        {
            if (ModelState.IsValid)
            {
                var action = _roleServices.DeleteRole(form, UserAuthorized);
                if (action.IsSuccess)
                {
                    return RedirectToAction("Index");
                }
            }

            return View(form);
        }
    }
}