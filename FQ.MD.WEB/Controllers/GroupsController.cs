﻿using FQ.MD.WEB.ViewModels.JSON;
using FQ.MD.WEB.Models;
using FQ.MD.WEB.Resources;
using FQ.MD.WEB.Services;
using FQ.MD.WEB.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using FQ.MD.WEB.ViewModels.Form;

namespace FQ.MD.WEB.Controllers
{
    [Authorize]
    public class GroupsController : BaseController
    {
        private GroupServices _groupServices = new GroupServices();
        private CompanyServices _companyServices = new CompanyServices();

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Index(DataTables model)
        {
            var searchKeywords = !String.IsNullOrEmpty(model.search.value) ? model.search.value.ToLower() : "";

            string sortBy = "GroupName";
            bool sortDir = true;

            if (model.order != null)
            {
                sortBy = model.columns[model.order[0].column].data;
                sortDir = model.order[0].dir.ToLower() == "asc";
            }

            Expression<Func<GT_MASTER_GROUPS, bool>> filter = u => u.GroupName.ToLower().Contains(searchKeywords) || u.GT_MASTER_COMPANIES.CompanyName.ToLower().Contains(searchKeywords);
            Func<IQueryable<GT_MASTER_GROUPS>, IOrderedQueryable<GT_MASTER_GROUPS>> orderBy = q => sortDir ? q.OrderBy(u => u.GroupName) : q.OrderByDescending(u => u.GroupName);

            if (sortBy == "Parent")
            {
                orderBy = q => sortDir ? q.OrderBy(u => u.GT_MASTER_GROUPS2.GroupName) : q.OrderByDescending(u => u.GT_MASTER_GROUPS2.GroupName);
            }

            if (sortBy == "Active")
            {
                orderBy = q => sortDir ? q.OrderBy(u => u.IsActive) : q.OrderByDescending(u => u.IsActive);
            }

            var models = _groupServices.Get(filter: filter, orderBy: orderBy, skip: model.start, take: model.length)
                .Select(c => new GroupViewModel
                {
                    GroupName = c.GroupName,
                    Parent = c.ParentId.HasValue ? c.GT_MASTER_GROUPS2.GroupName : "-",
                    Active = DisplayActiveIcon(c.IsActive),
                    Actions = DisplayActions(UserAuthorization, c.GroupId, null)
                });

            var totalResultsCount = _groupServices.Get().Count();
            var recordsFiltered = String.IsNullOrEmpty(model.search.value) ? totalResultsCount : _groupServices.Get(filter: filter).Count();

            return Json(new
            {
                draw = model.draw,
                recordsTotal = totalResultsCount,
                recordsFiltered = recordsFiltered,
                data = models.ToList()
            });
        }

        public ActionResult Add()
        {
            ViewBag.CompanyId = CompanyOptions(null);
            ViewBag.ParentId = GroupOptions(null);
            return View();
        }

        [HttpPost]
        public ActionResult Add(GroupAddForm form)
        {
            if (ModelState.IsValid)
            {
                var action = _groupServices.AddGroup(form, UserAuthorized);
                if (action.IsSuccess)
                {
                    return RedirectToAction("Index");
                }

                var message = DisplayMessage(action.Message, new string[] { "GroupName" });
                ModelState.AddModelError("", message);
            }

            ViewBag.CompanyId = CompanyOptions(form.CompanyId);
            ViewBag.ParentId = GroupOptions(form.ParentId);
            return View(form);
        }

        public ActionResult Edit(int Id)
        {
            var model = _groupServices.GetGroupById(Id);
            if (model == null)
            {
                return RedirectToAction("Index", "NotFound");
            }

            var form = new GroupEditForm();
            form.GroupId = model.GroupId;
            form.GroupName = model.GroupName;
            form.CompanyId = model.CompanyId;
            form.ParentId = model.ParentId;
            form.Note = model.Note;
            form.IsActive = model.IsActive;

            ViewBag.CompanyId = CompanyOptions(form.CompanyId);
            ViewBag.ParentId = GroupOptions(form.ParentId);
            return View(form);
        }

        [HttpPost]
        public ActionResult Edit(GroupEditForm form)
        {
            if (ModelState.IsValid)
            {
                var action = _groupServices.UpdateGroup(form, UserAuthorized);
                if (action.IsSuccess)
                {
                    return RedirectToAction("Index");
                }

                var message = DisplayMessage(action.Message, new string[] { "GroupName" });
                ModelState.AddModelError("", message);
            }

            ViewBag.CompanyId = CompanyOptions(form.CompanyId);
            ViewBag.ParentId = GroupOptions(form.ParentId);
            return View(form);
        }

        public ActionResult Detail(int Id)
        {
            var model = _groupServices.GetGroupById(Id);
            if (model == null)
            {
                return RedirectToAction("Index", "NotFound");
            }

            var viewModel = new GroupViewModel();
            viewModel.GroupId = model.GroupId;
            viewModel.GroupName = model.GroupName;
            viewModel.Company = model.GT_MASTER_COMPANIES.CompanyName;
            viewModel.Parent = model.ParentId.HasValue? model.GT_MASTER_GROUPS2.GroupName : "";
            viewModel.Note = model.Note;
            viewModel.Active = DisplayActiveIcon(model.IsActive);

            return View(viewModel);
        }

        public ActionResult Delete(int Id)
        {
            var model = _groupServices.GetGroupById(Id);
            if (model == null)
            {
                return RedirectToAction("Index", "NotFound");
            }

            var form = new GroupDeleteForm();
            form.GroupId = model.GroupId;
            form.GroupName = model.GroupName;
            form.CompanyId = model.CompanyId;
            form.ParentId = model.ParentId;
            form.Note = model.Note;
            form.IsActive = model.IsActive;

            ViewBag.CompanyId = CompanyOptions(form.CompanyId);
            ViewBag.ParentId = GroupOptions(form.ParentId);

            return View(form);
        }

        [HttpPost]
        public ActionResult Delete(GroupDeleteForm form)
        {
            if (ModelState.IsValid)
            {
                var action = _groupServices.DeleteGroup(form, UserAuthorized);
                if (action.IsSuccess)
                {
                    return RedirectToAction("Index");
                }
            }

            ViewBag.CompanyId = CompanyOptions(form.CompanyId);
            ViewBag.ParentId = GroupOptions(form.ParentId);
            return View(form);
        }

        private SelectList CompanyOptions(int? CompanyId)
        {
            var Companies = _companyServices.Get(filter: q => q.IsActive, orderBy: q => q.OrderBy(x => x.CompanyName))
                .Select(g => new SelectItemIntValue
                {
                    Value = g.CompanyId,
                    Text = g.CompanyName
                }).ToList();
            Companies.Insert(0, new SelectItemIntValue() { Value = null, Text = Res.SelectItemNull });
            return new SelectList(Companies, "Value", "Text", CompanyId);
        }

        private SelectList GroupOptions(int? GroupId)
        {
            var Groups = _groupServices.Get(filter: q => q.IsActive, orderBy: q => q.OrderBy(x => x.GroupName))
                .Select(g => new SelectItemIntValue
                {
                    Value = g.GroupId,
                    Text = g.GroupName
                }).ToList();
            Groups.Insert(0, new SelectItemIntValue() { Value = null, Text = Res.SelectItemNull });
            return new SelectList(Groups, "Value", "Text", GroupId);
        }
    }
}