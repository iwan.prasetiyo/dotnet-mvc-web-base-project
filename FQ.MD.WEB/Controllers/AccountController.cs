﻿using FQ.MD.WEB.Helpers;
using FQ.MD.WEB.Models;
using FQ.MD.WEB.Resources;
using FQ.MD.WEB.Services;
using FQ.MD.WEB.ViewModels;
using FQ.MD.WEB.ViewModels.Form;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace FQ.MD.WEB.Controllers
{
    public class AccountController : BaseController
    {
        private UserServices _userServices = new UserServices();
        private GroupServices _groupServices = new GroupServices();
        private StatusServices _statusServices = new StatusServices();
        private LogWebActivityServices _logActivityServices = new LogWebActivityServices();

        [Authorize]
        public ActionResult Index()
        {
            var viewModel = new AccountViewModel();
            viewModel.UserId = UserAuthorized.UserId;
            viewModel.Username = UserAuthorized.Username;
            viewModel.Name = UserAuthorized.Name;
            viewModel.Email = UserAuthorized.Email;
            viewModel.GroupName = UserAuthorized.GT_MASTER_GROUPS.GroupName;

            return View(viewModel);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Login(string returnUrl, string msgForgetPassword)
        {
            return View(new AccountLoginForm { returnUrl = returnUrl, msgForgetPassword = msgForgetPassword });
        }

        [HttpPost]
        public ActionResult Login(AccountLoginForm form)
        {
            if (ModelState.IsValid)
            {
                var loginAction = _userServices.Login(form.Username, form.Password);
                if (loginAction.IsSuccess)
                {
                    FormsAuthentication.SetAuthCookie(loginAction.Data.Username, true);

                    _logActivityServices.AddLog(new GT_LOG_WEB_ACTIVITIES
                    {
                        MenuId = 1,
                        Controller = "Account",
                        Action = "Login",
                        Method = "POST",
                        UserId = loginAction.Data.UserId,
                        CreatedDate = DateTime.Now
                    });

                    if (!string.IsNullOrEmpty(form.returnUrl))
                    {
                        return Redirect(form.returnUrl);
                    }
                    return RedirectToAction("Index", "Home");
                }
                ModelState.AddModelError("", loginAction.Message);
            }

            return View(form);
        }

        public ActionResult Logout()
        {
            _logActivityServices.AddLog(new GT_LOG_WEB_ACTIVITIES
            {
                MenuId = 2,
                Controller = "Account",
                Action = "Logout",
                Method = "GET",
                UserId = UserAuthorized.UserId,
                CreatedDate = DateTime.Now
            });
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "Account");
        }

        [Authorize]
        public ActionResult ChangePassword()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangePassword(AccountChangePasswordForm form)
        {
            var services = new UserServices();
            if (ModelState.IsValid)
            {
                var action = _userServices.ChangePassword(UserAuthorized.UserId, form.Password, form.NewPassword);
                if (action.IsSuccess)
                {
                    return RedirectToAction("Index");
                }

                ModelState.AddModelError("Password", action.Message);
            }
            return View(form);
        }

        private SelectList GroupOptions(int? GroupId)
        {
            var adminGroups = new int[] { 1, 2, 3 };
            var Groups = _groupServices.Get(filter: q => q.IsActive && !adminGroups.Contains(q.GroupId)).Select(g => new SelectItemIntValue
            {
                Value = g.GroupId,
                Text = g.GroupName
            }).ToList();
            Groups.Insert(0, new SelectItemIntValue() { Value = null, Text = Res.SelectItemNull });

            return new SelectList(Groups, "Value", "Text", GroupId);
        }
    }
}