﻿using FQ.MD.WEB.Resources;
using FQ.MD.WEB.Services;
using FQ.MD.WEB.ViewModels;
using FQ.MD.WEB.ViewModels.Form;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FQ.MD.WEB.Controllers
{
    public class UsersRolesController : BaseController
    {
        private UserServices _userServices = new UserServices();
        private GroupServices _groupServices = new GroupServices();
        private UserRoleServices _userRoleServices = new UserRoleServices();
        private RoleServices _roleServices = new RoleServices();

        public ActionResult Index(UserRoleIndexViewModel viewModel)
        {
            var user = _userServices.GetUserById(viewModel.UserId);
            if (user == null)
            {
                return RedirectToAction("Index", "NotFound");
            }

            var roles = new List<UserRoleViewModel>();
            foreach (var userRole in user.GT_MASTER_USER_ROLES.Where(q => !q.IsDeleted && !q.GT_MASTER_ROLES.IsDeleted).Select(q => q))
            {
                roles.Add(new UserRoleViewModel
                {
                    UserRoleId = userRole.UserRoleId,
                    Role = userRole.GT_MASTER_ROLES.Role,
                    Active = DisplayActiveIcon(userRole.IsActive),
                    Actions = DisplayActions(UserAuthorization, userRole.UserRoleId, null)
                });
            }

            viewModel.Name = user.Name;
            viewModel.Group = user.GT_MASTER_GROUPS.GroupName;
            viewModel.Roles = roles.OrderBy(q => q.Role).ToList();

            return View(viewModel);
        }

        public ActionResult Add(int UserId)
        {
            var user = _userServices.GetUserById(UserId);
            if (user == null)
            {
                return RedirectToAction("Index", "NotFound");
            }

            var form = new UserRoleAddForm();
            form.UserId = user.UserId;
            form.IsActive = true;

            ViewBag.RoleId = RoleOptions(null);
            ViewBag.Name = user.Name;
            ViewBag.Group = user.GT_MASTER_GROUPS.GroupName;

            return View(form);
        }

        [HttpPost]
        public ActionResult Add(UserRoleAddForm form)
        {
            if (ModelState.IsValid)
            {
                var action = _userRoleServices.AddUserRole(form, UserAuthorized);
                if (action.IsSuccess)
                {
                    return RedirectToAction("Index", new { UserId = form.UserId });
                }

                var message = DisplayMessage(action.Message, new string[] { "RoleId" });
                ModelState.AddModelError("", message);
            }

            ViewBag.RoleId = RoleOptions(form.RoleId);

            var user = _userServices.GetUserById(form.UserId);
            ViewBag.Name = user.Name;
            ViewBag.Group = user.GT_MASTER_GROUPS.GroupName;

            return View(form);
        }

        public ActionResult Edit(int Id)
        {
            var model = _userRoleServices.GetUserRoleById(Id);
            if (model == null)
            {
                return RedirectToAction("Index", "NotFound");
            }

            var viewModel = new UserRoleEditForm();
            viewModel.UserRoleId = model.UserRoleId;
            viewModel.UserId = model.UserId;
            viewModel.RoleId = model.RoleId;
            viewModel.IsActive = model.IsActive;

            ViewBag.RoleId = RoleOptions(viewModel.RoleId);

            var user = model.GT_MASTER_USERS;
            ViewBag.Name = user.Name;
            ViewBag.Group = user.GT_MASTER_GROUPS.GroupName;

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Edit(UserRoleEditForm form)
        {
            if (ModelState.IsValid)
            {
                var action = _userRoleServices.UpdateUserRole(form, UserAuthorized);
                if (action.IsSuccess)
                {
                    return RedirectToAction("Index", new { UserId = form.UserId });
                }

                var message = DisplayMessage(action.Message, new string[] { "RoleId" });
                ModelState.AddModelError("", message);
            }

            ViewBag.RoleId = RoleOptions(form.RoleId);

            var user = _userServices.GetUserById(form.UserId);
            ViewBag.Name = user.Name;
            ViewBag.Group = user.GT_MASTER_GROUPS.GroupName;

            return View(form);
        }

        public ActionResult Detail(int Id)
        {
            var model = _userRoleServices.GetUserRoleById(Id);
            if (model == null)
            {
                return RedirectToAction("Index", "NotFound");
            }

            var viewModel = new UserRoleViewModel();
            viewModel.UserRoleId = model.UserRoleId;
            viewModel.Role = model.GT_MASTER_ROLES.Role;
            viewModel.Active = DisplayActiveIcon(model.IsActive);

            var user = model.GT_MASTER_USERS;
            ViewBag.UserId = user.UserId;
            ViewBag.Name = user.Name;
            ViewBag.Group = user.GT_MASTER_GROUPS.GroupName;

            return View(viewModel);
        }

        public ActionResult Delete(int Id)
        {
            var model = _userRoleServices.GetUserRoleById(Id);
            if (model == null)
            {
                return RedirectToAction("Index", "NotFound");
            }

            var form = new UserRoleDeleteForm();
            form.UserRoleId = model.UserRoleId;
            form.UserId = model.UserId;
            form.RoleId = model.RoleId;
            form.IsActive = model.IsActive;

            ViewBag.RoleId = RoleOptions(form.RoleId);

            var user = model.GT_MASTER_USERS;
            ViewBag.Name = user.Name;
            ViewBag.Group = user.GT_MASTER_GROUPS.GroupName;

            return View(form);
        }

        [HttpPost]
        public ActionResult Delete(UserRoleDeleteForm form)
        {
            if (ModelState.IsValid)
            {
                var action = _userRoleServices.DeleteUserRole(form, UserAuthorized);
                if (action.IsSuccess)
                {
                    return RedirectToAction("Index", new { UserId = form.UserId });
                }

                var message = DisplayMessage(action.Message, null);
                ModelState.AddModelError("", message);
            }

            ViewBag.RoleId = RoleOptions(form.RoleId);

            var model = _userRoleServices.GetUserRoleById(form.UserRoleId);
            var user = model.GT_MASTER_USERS;
            ViewBag.Name = user.Name;
            ViewBag.Group = user.GT_MASTER_GROUPS.GroupName;

            return View(form);
        }

        private SelectList RoleOptions(int? MenuId)
        {
            var Options = _roleServices.Get(filter: q => q.IsActive).Select(g => new SelectItemIntValue
            {
                Value = g.RoleId,
                Text = g.Role
            }).ToList();
            Options.Insert(0, new SelectItemIntValue() { Value = null, Text = Res.SelectItemNull });

            return new SelectList(Options, "Value", "Text", MenuId);
        }
    }
}