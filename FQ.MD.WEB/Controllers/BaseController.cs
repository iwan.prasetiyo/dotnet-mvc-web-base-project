﻿using FQ.MD.WEB.Helpers;
using FQ.MD.WEB.Models;
using FQ.MD.WEB.Resources;
using FQ.MD.WEB.Services;
using FQ.MD.WEB.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;

namespace FQ.MD.WEB.Controllers
{
    public class BaseController : Controller
    {
        public GT_MASTER_USERS UserAuthorized;
        public AuthorizationResult UserAuthorization;
        private LogErrorServices _logErrorServices = new LogErrorServices();
        private LogWebActivityServices _logActivityServices = new LogWebActivityServices();

        public string DisplayMessage(string DisplayKey, string[] ParamKeys)
        {
            var BuildStatus = ConfigurationManager.AppSettings["BUILD"];
            if (BuildStatus == "DEV")
            {
                var error = _logErrorServices.GetLastError();
                return error.ErrorMessage;
            }

            if (ParamKeys == null)
            {
                ParamKeys = new string[] { };
            }

            string[] Params = ParamKeys != null ? new string[ParamKeys.Count()] : new string[] { };

            for (int i = 0; i < ParamKeys.Count(); i++)
            {
                Params[i] = CultureHelper.GetResourceByCultureId(ParamKeys[i], CultureHelper.CurrentCulture);
            }

            return CultureHelper.GetResourceByCultureId(DisplayKey, CultureHelper.CurrentCulture).Replace("{p}", string.Join(", ", Params));
        }

        public string DisplayActiveIcon(bool IsActive)
        {
            return IsActive ? "<i class='fas fa-check color-brand'></i>" : "<i class='fas fa-minus text-muted'></i>";
        }

        public string DisplayActions(AuthorizationResult Authorization, int Id, List<ActionsSubmenu> Submenus = null)
        {
            var Actions = "";
            if (Submenus != null && Submenus.Count > 0)
            {
                foreach (var submenu in Submenus)
                {
                    //**Display Submenu**//
                    var CustomLabel = CultureHelper.GetResourceByCultureId(submenu.SubmenuLabel, CultureHelper.CurrentCulture);
                    Actions += "<a href='/" + submenu.SubmenuController + (!string.IsNullOrEmpty(submenu.SubmenuKey) ? ("?" + submenu.SubmenuKey + "=" + Id) : "") + "' title='" + CustomLabel + "' class='btn btn-sm btn-primary'>" + CustomLabel + "</a> ";
                }
            }
            //**Display Show**//
            Actions += Authorization.IsReadGranted ? "<a href='" + Url.Action("Detail", new { Id = Id }) + "' title='" + Res.Show + "' class='btn btn-sm btn-primary'><i class='far fa-eye'></i></a> " : "";
            //**Display Edit**//
            Actions += Authorization.IsUpdateGranted ? "<a href='" + Url.Action("Edit", new { Id = Id }) + "' title='" + Res.Edit + "' class='btn btn-sm btn-warning'><i class='fas fa-edit'></i></a> " : "";
            //**Display Delete**//
            Actions += Authorization.IsDeleteGranted ? "<a href='" + Url.Action("Delete", new { Id = Id }) + "' title='" + Res.Edit + "' class='btn btn-sm btn-danger'><i class='fas fa-trash-alt'></i></a> " : "";
            return Actions;
        }

        public string[] DisplayErrorArray(ModelStateDictionary ModelState)
        {
            var errors = ModelState.Where(q => q.Value.Errors.Count > 0).Select(q => new { Key = q.Key, Message = q.Value.Errors.Select(x => x.ErrorMessage).ToArray() }).ToList();

            return errors.Select(q => q.Key + " " + q.Message).ToArray();
        }

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);

            UserAuthorized = new UserServices().Get(filter: q => q.Username.ToLower() == User.Identity.Name.ToLower()).FirstOrDefault();
        }

        protected override void ExecuteCore()
        {
            int culture = 0;
            if (this.Session == null || this.Session["CurrentCulture"] == null)
            {
                var userLanguages = Request.UserLanguages[0];
                int.TryParse(System.Configuration.ConfigurationManager.AppSettings["Culture"], out culture);
                this.Session["CurrentCulture"] = userLanguages.Contains("ID") ? 1 : culture;
            }
            else
            {
                culture = (int)this.Session["CurrentCulture"];
            }
            CultureHelper.CurrentCulture = culture;

            base.ExecuteCore();
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            var BuildStatus = ConfigurationManager.AppSettings["BUILD"];
            var errorLog = _logErrorServices.AddLog(filterContext.Exception, UserAuthorized != null ? UserAuthorized.UserId : (int?)null);
            if (BuildStatus != "DEV")
            {
                if (filterContext.ExceptionHandled)
                {
                    return;
                }
                filterContext.Result = new RedirectResult("/ServerError?DisplayKey=" + errorLog.Message);
                filterContext.ExceptionHandled = true;
            }
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var controllerNotLogged = new string[] { "Home", "Account", "NotFound", "ServerError", "Unauthorized", "Security" };

            var contoller = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            var action = filterContext.ActionDescriptor.ActionName;
            var method = Request.HttpMethod;

            if (UserAuthorized == null && User.Identity.Name != "")
            {
                FormsAuthentication.SignOut();
                var unknownAccess = new UrlHelper(filterContext.RequestContext).Action("Login", "Account");
                filterContext.Result = new RedirectResult(unknownAccess);
                return;
            }

            if (UserAuthorized != null && !controllerNotLogged.Contains(contoller))
            {
                var menuHelper = new MenuHelpers();

                UserAuthorization = menuHelper.IsAuthorized(UserAuthorized, contoller, action);
                if (!UserAuthorization.IsAthorized)
                {
                    var unAuthorizedUrl = new UrlHelper(filterContext.RequestContext).Action("Index", "Unauthorized");
                    filterContext.Result = new RedirectResult(unAuthorizedUrl);
                    return;
                }

                ViewBag.IsCreateGranted = UserAuthorization.IsCreateGranted;
                ViewBag.IsSyncGranted = UserAuthorization.IsSyncGranted;
            }

            _logActivityServices.AddLog(new GT_LOG_WEB_ACTIVITIES
            {
                MenuId = UserAuthorization != null ? UserAuthorization.MenuId : (int?)null,
                Controller = contoller,
                Action = action,
                Method = method,
                UserId = UserAuthorized != null ? UserAuthorized.UserId : (int?)null,
                CreatedDate = DateTime.Now
            });

            ViewBag.ContollerName = contoller;
            ViewBag.User = UserAuthorized;
            base.OnActionExecuting(filterContext);
        }

        protected override bool DisableAsyncSupport
        {
            get { return true; }
        }
    }
}