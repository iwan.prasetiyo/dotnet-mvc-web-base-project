﻿using FQ.MD.WEB.Helpers;
using FQ.MD.WEB.Models;
using FQ.MD.WEB.Services;
using FQ.MD.WEB.ViewModels;
using FQ.MD.WEB.ViewModels.JSON;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace FQ.MD.WEB.Controllers
{
    public class HomeController : BaseController
    {
        private LogWebActivityServices _webActivityServices = new LogWebActivityServices();

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        [Authorize]
        public ActionResult Index()
        {
            ViewBag.IsAdmin = UserAuthorized.GT_MASTER_USER_ROLES.Where(q => q.RoleId == 1 || q.RoleId == 2).Count() > 0;

            return View();
        }

        public ActionResult ChangeCurrentCulture(int id)
        {
            var CurrentCultureId = Session["CurrentCulture"];
            CultureHelper.CurrentCulture = id;
            Session["CurrentCulture"] = id;
            return Redirect(Request.UrlReferrer.ToString());
        }

        public JsonResult GetAdminChartData(RestResponse<HomeChatrsViewModel> viewModel)
        {
            var response = new RestResponse<HomeChatrsViewModel>() { IsSuccess = false };

            var currentMonth = DateTime.Now.ToString("MMM-yyyy", CultureInfo.InvariantCulture);
            var startDate = DateTime.Parse("1-" + currentMonth, CultureInfo.InvariantCulture);
            var endDate = startDate.AddMonths(1);

            Expression<Func<GT_LOG_WEB_ACTIVITIES, bool>> filter = q => q.CreatedDate >= startDate && q.CreatedDate < endDate;
            var models = _webActivityServices.Get(filter: filter).GroupBy(q => q.CreatedDate.Date).Select(q => new HomeChartDataViewModel { Date = q.Key.ToString("dd"), Count = q.Count() });
            var dates = Enumerable.Range(startDate.Day, endDate.AddDays(-1).Day).Select(q => q.ToString())
                .Select(q => new HomeChartDataViewModel
                {
                    Date = q,
                    Count = models.Where(x => x.Date == q).Count() != 0 ? models.Where(x => x.Date == q).FirstOrDefault().Count : 0
                });

            response.IsSuccess = true;
            response.Data = new HomeChatrsViewModel
            {
                Label = dates.Select(q => q.Date).ToArray(),
                Count = dates.Select(q => q.Count).ToArray()
            };

            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}