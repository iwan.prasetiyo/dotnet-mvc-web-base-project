﻿using FQ.MD.WEB.Models;
using FQ.MD.WEB.Resources;
using FQ.MD.WEB.Services;
using FQ.MD.WEB.ViewModels;
using FQ.MD.WEB.ViewModels.Form;
using FQ.MD.WEB.ViewModels.JSON;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;

namespace FQ.MD.WEB.Controllers
{
    [Authorize]
    public class MenusController : BaseController
    {
        private MenuServices _services = new MenuServices();

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Index(DataTables model)
        {
            var searchKeywords = !String.IsNullOrEmpty(model.search.value) ? model.search.value.ToLower() : "";

            string sortBy = "Key";
            bool sortDir = true;

            if (model.order != null)
            {
                sortBy = model.columns[model.order[0].column].data;
                sortDir = model.order[0].dir.ToLower() == "asc";
            }

            Expression<Func<GT_MASTER_MENUS, bool>> filter = u => u.Key.ToLower().Contains(searchKeywords) || u.URL.ToLower().Contains(searchKeywords);
            Func<IQueryable<GT_MASTER_MENUS>, IOrderedQueryable<GT_MASTER_MENUS>> orderBy = q => sortDir ? q.OrderBy(u => u.Key) : q.OrderByDescending(u => u.Key);
            if (sortBy == "Parent")
            {
                orderBy = q => sortDir ? q.OrderBy(u => u.GT_MASTER_MENUS2.Key) : q.OrderByDescending(u => u.GT_MASTER_MENUS2.Key);
            }

            if (sortBy == "Url")
            {
                orderBy = q => sortDir ? q.OrderBy(u => u.URL) : q.OrderByDescending(u => u.URL);
            }

            if (sortBy == "Sequence")
            {
                orderBy = q => sortDir ? q.OrderBy(u => u.Sequence) : q.OrderByDescending(u => u.Sequence);
            }

            if (sortBy == "Active")
            {
                orderBy = q => sortDir ? q.OrderBy(u => u.IsActive) : q.OrderByDescending(u => u.IsActive);
            }

            var models = _services.Get(filter: filter, orderBy: orderBy, skip: model.start, take: model.length)
                .Select(c => new MenuViewModel
                {
                    Key = c.Key,
                    Parent = c.ParentId.HasValue ? c.GT_MASTER_MENUS2.Key : null,
                    Url = c.URL,
                    Sequence = c.Sequence,
                    Active = DisplayActiveIcon(c.IsActive),
                    Actions = DisplayActions(UserAuthorization, c.MenuId, null)
                });

            var totalResultsCount = _services.Get().Count();
            var recordsFiltered = String.IsNullOrEmpty(model.search.value) ? totalResultsCount : _services.Get(filter: filter).Count();

            return Json(new
            {
                draw = model.draw,
                recordsTotal = totalResultsCount,
                recordsFiltered = recordsFiltered,
                data = models.ToList()
            });
        }

        public ActionResult Add()
        {
            ViewBag.ParentId = ParentOptions(null);
            return View();
        }

        [HttpPost]
        public ActionResult Add(MenuAddForm form)
        {
            if (ModelState.IsValid)
            {
                var action = _services.AddMenu(form, UserAuthorized);
                if (action.IsSuccess)
                {
                    return RedirectToAction("Index");
                }

                var message = DisplayMessage(action.Message, new string[] { "Key" });
                ModelState.AddModelError("", message);
            }

            ViewBag.ParentId = ParentOptions(form.ParentId);
            return View(form);
        }

        public ActionResult Edit(int Id)
        {
            var model = _services.GetMenuById(Id);
            if (model == null)
            {
                return RedirectToAction("Index", "NotFound");
            }

            var form = new MenuEditForm();
            form.MenuId = model.MenuId;
            form.Key = model.Key;
            form.Description = model.Description;
            form.Url = model.URL;
            form.Icon = model.Icon;
            form.ParentId = model.ParentId;
            form.Sequence = model.Sequence;
            form.IsActive = model.IsActive;

            ViewBag.ParentId = ParentOptions(form.ParentId);
            return View(form);
        }

        [HttpPost]
        public ActionResult Edit(MenuEditForm from)
        {
            if (ModelState.IsValid)
            {
                var action = _services.UpdateMenu(from, UserAuthorized);
                if (action.IsSuccess)
                {
                    return RedirectToAction("Index");
                }
            }

            ViewBag.ParentId = ParentOptions(from.ParentId);
            return View(from);
        }

        public ActionResult Detail(int Id)
        {
            var model = _services.GetMenuById(Id);
            if (model == null)
            {
                return RedirectToAction("Index", "NotFound");
            }

            var viewModel = new MenuViewModel();
            viewModel.MenuId = model.MenuId;
            viewModel.Key = model.Key;
            viewModel.Description = model.Description;
            viewModel.Url = model.URL;
            viewModel.Icon = model.Icon;
            viewModel.Sequence = model.Sequence;
            viewModel.Parent = model.ParentId.HasValue ? model.GT_MASTER_MENUS2.Key : null;
            viewModel.Active = DisplayActiveIcon(model.IsActive);

            return View(viewModel);
        }

        public ActionResult Delete(int Id)
        {
            var model = _services.GetMenuById(Id);
            if (model == null)
            {
                return RedirectToAction("Index", "NotFound");
            }

            var form = new MenuDeleteForm();
            form.MenuId = model.MenuId;
            form.Key = model.Key;
            form.Description = model.Description;
            form.Url = model.URL;
            form.Icon = model.Icon;
            form.Sequence = model.Sequence;
            form.ParentId = model.ParentId;
            form.IsActive = model.IsActive;

            ViewBag.ParentId = ParentOptions(form.ParentId);
            return View(form);
        }

        [HttpPost]
        public ActionResult Delete(MenuDeleteForm form)
        {
            if (ModelState.IsValid)
            {
                var action = _services.DeleteMenu(form, UserAuthorized);
                if (action.IsSuccess)
                {
                    return RedirectToAction("Index");
                }
            }

            ViewBag.ParentId = ParentOptions(form.ParentId);
            return View(form);
        }

        private SelectList ParentOptions(int? MenuId)
        {
            var Groups = _services.Get(filter: q => !q.IsLink && q.IsActive).Select(g => new SelectItemIntValue
            {
                Value = g.MenuId,
                Text = g.Key
            }).ToList();
            Groups.Insert(0, new SelectItemIntValue() { Value = null, Text = Res.SelectItemNull });

            return new SelectList(Groups, "Value", "Text", MenuId);
        }
    }
}