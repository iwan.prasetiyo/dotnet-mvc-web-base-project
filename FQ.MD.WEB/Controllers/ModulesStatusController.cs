﻿using FQ.MD.WEB.Services;
using FQ.MD.WEB.ViewModels;
using FQ.MD.WEB.ViewModels.Form;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FQ.MD.WEB.Controllers
{
    public class ModulesStatusController : BaseController
    {
        private ModuleServices _moduleServices = new ModuleServices();
        private StatusServices _statusServices = new StatusServices();

        public ActionResult Index(ModuleViewModel viewModel)
        {
            var model = _moduleServices.GetModuleById(viewModel.ModuleId);
            if (model == null)
            {
                return RedirectToAction("Index", "NotFound");
            }

            viewModel.Module = model.Module;
            viewModel.Status = model.GT_MASTER_STATUS.Where(q => !q.IsDeleted).Select(q => new StatusViewModel
            {
                StatusId = q.StatusId,
                Status = q.Status,
                Description = q.Description,
                Actions = DisplayActions(UserAuthorization, q.StatusId)
            }).ToList();

            return View(viewModel);
        }

        public ActionResult Add(int ModuleId)
        {
            var module = _moduleServices.GetModuleById(ModuleId);
            if (module == null)
            {
                return RedirectToAction("Index", "NotFound");
            }

            var form = new StatusAddForm();
            form.ModuleId = module.ModuleId;

            ViewBag.Module = module.Module;

            return View(form);
        }

        [HttpPost]
        public ActionResult Add(StatusAddForm form)
        {
            if (ModelState.IsValid)
            {
                var action = _statusServices.AddStatus(form, UserAuthorized);
                if (action.IsSuccess)
                {
                    return RedirectToAction("Index", new { ModuleId = form.ModuleId });
                }

                var message = DisplayMessage(action.Message, new string[] { "Status" });
                ModelState.AddModelError("", message);
            }

            var module = _moduleServices.GetModuleById(form.ModuleId);
            ViewBag.Module = module.Module;
            return View(form);
        }

        public ActionResult Edit(int Id)
        {
            var model = _statusServices.GetStatusById(Id);
            if (model == null)
            {
                return RedirectToAction("Index", "NotFound");
            }

            var form = new StatusEditForm();
            form.StatusId = model.StatusId;
            form.Status = model.Status;
            form.ModuleId = model.ModuleId;
            form.Description = model.Description;

            var module = model.GT_MASTER_MODULES;
            ViewBag.Module = module.Module;
            return View(form);
        }

        [HttpPost]
        public ActionResult Edit(StatusEditForm form)
        {
            if (ModelState.IsValid)
            {
                var action = _statusServices.UpdateStatus(form, UserAuthorized);
                if (action.IsSuccess)
                {
                    return RedirectToAction("Index", new { ModuleId = form.ModuleId });
                }

                var message = DisplayMessage(action.Message, new string[] { "Status" });
                ModelState.AddModelError("", message);
            }

            var status = _statusServices.GetStatusById(form.StatusId);
            var module = status.GT_MASTER_MODULES;
            ViewBag.Module = module.Module;

            return View(form);
        }

        public ActionResult Detail(int Id)
        {
            var model = _statusServices.GetStatusById(Id);
            if (model == null)
            {
                return RedirectToAction("Index", "NotFound");
            }

            var viewModel = new StatusViewModel();
            viewModel.StatusId = model.StatusId;
            viewModel.Status = model.Status;
            viewModel.Description = model.Description;

            var module = model.GT_MASTER_MODULES;
            ViewBag.Module = module.Module;
            ViewBag.ModuleId = module.ModuleId;

            return View(viewModel);
        }

        public ActionResult Delete(int Id)
        {
            var model = _statusServices.GetStatusById(Id);
            if (model == null)
            {
                return RedirectToAction("Index", "NotFound");
            }

            var form = new StatusDeleteForm();
            form.StatusId = model.StatusId;
            form.ModuleId = model.ModuleId;
            form.Status = model.Status;
            form.Description = model.Description;

            var module = model.GT_MASTER_MODULES;
            ViewBag.Module = module.Module;

            return View(form);
        }

        [HttpPost]
        public ActionResult Delete(StatusDeleteForm form)
        {
            if (ModelState.IsValid)
            {
                var action = _statusServices.DeletedStatus(form, UserAuthorized);
                if (action.IsSuccess)
                {
                    return RedirectToAction("IndexStatus", new { ModuleId = form.ModuleId });
                }

                var message = DisplayMessage(action.Message, null);
                ModelState.AddModelError("", message);
            }

            var model = _statusServices.GetStatusById(form.StatusId);
            var module = model.GT_MASTER_MODULES;
            ViewBag.Module = module.Module;

            return View(form);
        }
    }
}