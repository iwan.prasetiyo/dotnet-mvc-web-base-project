﻿using FQ.MD.WEB.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FQ.MD.WEB.Controllers
{
    public class UnauthorizedController : BaseController
    {
        public ActionResult Index()
        {
            ViewBag.Message = Res.Unauthorized;
            return View();
        }
    }
}