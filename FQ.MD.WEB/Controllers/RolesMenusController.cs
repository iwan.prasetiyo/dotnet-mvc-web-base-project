﻿using FQ.MD.WEB.Helpers;
using FQ.MD.WEB.Resources;
using FQ.MD.WEB.Services;
using FQ.MD.WEB.ViewModels;
using FQ.MD.WEB.ViewModels.Form;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FQ.MD.WEB.Controllers
{
    public class RolesMenusController : BaseController
    {
        private RoleServices _roleServices = new RoleServices();
        private RoleMenuServices _roleMenuServices = new RoleMenuServices();
        private MenuServices _menuServices = new MenuServices();

        public ActionResult Index(RoleMenuIndexViewModel viewModel)
        {
            var role = _roleServices.GetRoleById(viewModel.RoleId);
            if (role == null)
            {
                return RedirectToAction("Index", "NotFound");
            }

            var menus = new List<RoleMenuViewModel>();
            var roleMenus = role.GT_MASTER_ROLE_MENUS.Where(q => !q.IsDeleted && q.GT_MASTER_MENUS.IsActive && !q.GT_MASTER_MENUS.IsDeleted);
            foreach (var roleMenu in roleMenus)
            {
                if (roleMenu.GT_MASTER_MENUS.ParentId.HasValue && menus.Where(q => q.MenuId == roleMenu.GT_MASTER_MENUS.ParentId.Value).Count() == 0)
                {
                    var parent = _menuServices.GetMenuById(roleMenu.GT_MASTER_MENUS.ParentId.Value);
                    menus.Add(new RoleMenuViewModel
                    {
                        MenuId = parent.MenuId,
                        Menu = parent.Key,
                        Sequence = parent.Sequence,
                        Display = "<div class=\"menu-icon\"><i class=\" menu-icon " + parent.Icon + "\"></i></div>" + CultureHelper.GetResourceByCultureId(parent.Key, CultureHelper.CurrentCulture)
                    });
                }

                menus.Add(new RoleMenuViewModel
                {
                    Id = roleMenu.RoleMenuId,
                    ParentMenuId = roleMenu.GT_MASTER_MENUS.ParentId,
                    Menu = roleMenu.GT_MASTER_MENUS.Key,
                    IsLink = roleMenu.GT_MASTER_MENUS.IsLink,
                    Sequence = roleMenu.GT_MASTER_MENUS.Sequence,
                    Display = "<div class=\"menu-icon\"><i class=\"" + roleMenu.GT_MASTER_MENUS.Icon + "\"></i></div>" + CultureHelper.GetResourceByCultureId(roleMenu.GT_MASTER_MENUS.Key, CultureHelper.CurrentCulture),
                    Read = DisplayActiveIcon(roleMenu.GrantRead),
                    Create = DisplayActiveIcon(roleMenu.GrantCreate),
                    Update = DisplayActiveIcon(roleMenu.GrantUpdate),
                    Delete = DisplayActiveIcon(roleMenu.GrantDelete),
                    Sync = DisplayActiveIcon(roleMenu.GrantSync),
                    Upload = DisplayActiveIcon(roleMenu.GrantUpload),
                    Active = DisplayActiveIcon(roleMenu.IsActive),
                    Actions = DisplayActions(UserAuthorization, roleMenu.RoleMenuId)
                });
            }

            viewModel.Role = role.Role;
            viewModel.Description = role.Description;
            viewModel.Menus = menus;

            return View(viewModel);
        }

        public ActionResult Add(int RoleId)
        {
            var role = _roleServices.GetRoleById(RoleId);
            if (role == null)
            {
                return RedirectToAction("Index", "NotFound");
            }

            var form = new RoleMenuAddForm();
            form.RoleId = role.RoleId;
            form.GrantRead = true;
            form.IsActive = true;

            ViewBag.MenuId = MenuOptions(null);
            ViewBag.Role = role.Role;

            return View(form);
        }

        [HttpPost]
        public ActionResult Add(RoleMenuAddForm form)
        {
            if (ModelState.IsValid)
            {
                var action = _roleMenuServices.AddRoleMenu(form, UserAuthorized);
                if (action.IsSuccess)
                {
                    return RedirectToAction("Index", new { RoleId = form.RoleId });
                }

                var message = DisplayMessage(action.Message, new string[] { "MenuId" });
                ModelState.AddModelError("", message);
            }

            ViewBag.MenuId = MenuOptions(form.MenuId);

            var role = _roleServices.GetRoleById(form.RoleId);
            ViewBag.Role = role.Role;

            return View(form);
        }

        public ActionResult Edit(int Id)
        {
            var model = _roleMenuServices.GetRoleMenuById(Id);
            if (model == null)
            {
                return RedirectToAction("Index", "NotFound");
            }

            var form = new RoleMenuEditForm();
            form.RoleMenuId = model.RoleMenuId;
            form.RoleId = model.RoleId;
            form.MenuId = model.MenuId;
            form.GrantCreate = model.GrantCreate;
            form.GrantDelete = model.GrantDelete;
            form.GrantRead = model.GrantRead;
            form.GrantUpdate = model.GrantUpdate;
            form.GrantUpload = model.GrantUpload;
            form.GrantSync = model.GrantSync;
            form.IsActive = model.IsActive;

            ViewBag.MenuId = MenuOptions(form.MenuId);

            var role = model.GT_MASTER_ROLES;
            ViewBag.Role = role.Role;

            return View(form);
        }

        [HttpPost]
        public ActionResult Edit(RoleMenuEditForm form)
        {
            if (ModelState.IsValid)
            {
                var action = _roleMenuServices.UpdateRoleMenu(form, UserAuthorized);
                if (action.IsSuccess)
                {
                    return RedirectToAction("Index", new { RoleId = form.RoleId });
                }

                var message = DisplayMessage(action.Message, new string[] { "MenuId" });
                ModelState.AddModelError("", message);
            }

            ViewBag.MenuId = MenuOptions(form.MenuId);

            var RoleMenu = _roleMenuServices.GetRoleMenuById(form.RoleMenuId);
            var role = RoleMenu.GT_MASTER_ROLES;
            ViewBag.Role = role.Role;

            return View(form);
        }

        public ActionResult Detail(int Id)
        {
            var model = _roleMenuServices.GetRoleMenuById(Id);
            if (model == null)
            {
                return RedirectToAction("Index", "NotFound");
            }

            var viewModel = new RoleMenuViewModel();
            viewModel.Id = model.RoleMenuId;
            viewModel.RoleId = model.RoleId;
            viewModel.Role = model.GT_MASTER_ROLES.Role;
            viewModel.MenuId = model.MenuId;
            viewModel.Menu = model.GT_MASTER_MENUS.Key;
            viewModel.Create = DisplayActiveIcon(model.GrantCreate);
            viewModel.Delete = DisplayActiveIcon(model.GrantDelete);
            viewModel.Read = DisplayActiveIcon(model.GrantRead);
            viewModel.Update = DisplayActiveIcon(model.GrantUpdate);
            viewModel.Active = DisplayActiveIcon(model.IsActive);

            return View(viewModel);
        }

        public ActionResult Delete(int Id)
        {
            var model = _roleMenuServices.GetRoleMenuById(Id);
            if (model == null)
            {
                return RedirectToAction("Index", "NotFound");
            }

            var form = new RoleMenuDeleteForm();
            form.RoleMenuId = model.RoleMenuId;
            form.RoleId = model.RoleId;
            form.MenuId = model.MenuId;
            form.GrantCreate = model.GrantCreate;
            form.GrantDelete = model.GrantDelete;
            form.GrantRead = model.GrantRead;
            form.GrantUpdate = model.GrantUpdate;
            form.IsActive = model.IsActive;

            ViewBag.MenuId = MenuOptions(form.MenuId);

            var role = model.GT_MASTER_ROLES;
            ViewBag.Role = role.Role;

            return View(form);
        }

        [HttpPost]
        public ActionResult Delete(RoleMenuDeleteForm form)
        {
            if (ModelState.IsValid)
            {
                var action = _roleMenuServices.DeleteRoleMenu(form, UserAuthorized);
                if (action.IsSuccess)
                {
                    return RedirectToAction("Index", new { RoleId = form.RoleId });
                }

                var message = DisplayMessage(action.Message, null);
                ModelState.AddModelError("", message);
            }

            ViewBag.MenuId = MenuOptions(form.MenuId);

            var roleMenu = _roleMenuServices.GetRoleMenuById(form.RoleMenuId);
            var role = roleMenu.GT_MASTER_ROLES;
            ViewBag.Role = role.Role;

            return View(form);
        }

        private SelectList MenuOptions(int? MenuId)
        {
            var Options = _menuServices.Get(filter: q => q.IsActive && q.IsLink).Select(g => new SelectItemIntValue
            {
                Value = g.MenuId,
                Text = g.Key
            }).ToList();
            Options.Insert(0, new SelectItemIntValue() { Value = null, Text = Res.SelectItemNull });

            return new SelectList(Options, "Value", "Text", MenuId);
        }
    }
}