﻿using FQ.MD.WEB.ViewModels.JSON;
using FQ.MD.WEB.Models;
using FQ.MD.WEB.Resources;
using FQ.MD.WEB.Services;
using FQ.MD.WEB.ViewModels;
using FQ.MD.WEB.ViewModels.Form;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;

namespace FQ.MD.WEB.Controllers
{
    [Authorize]
    public class ModulesController : BaseController
    {
        private ModuleServices _moduleServices = new ModuleServices();

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Index(DataTables model)
        {
            var searchKeywords = !String.IsNullOrEmpty(model.search.value) ? model.search.value.ToLower() : "";

            string sortBy = "Module";
            bool sortDir = true;

            if (model.order != null)
            {
                sortBy = model.columns[model.order[0].column].data;
                sortDir = model.order[0].dir.ToLower() == "asc";
            }

            Expression<Func<GT_MASTER_MODULES, bool>> filter = u => u.Module.ToLower().Contains(searchKeywords);
            Func<IQueryable<GT_MASTER_MODULES>, IOrderedQueryable<GT_MASTER_MODULES>> orderBy = q => sortDir ? q.OrderBy(u => u.Module) : q.OrderByDescending(u => u.Module);

            var submenus = new List<ActionsSubmenu>() { new ActionsSubmenu() { SubmenuLabel = "Status", SubmenuKey = "ModuleId", SubmenuController = "ModulesStatus" } };
            var models = _moduleServices.Get(filter: filter, orderBy: orderBy, skip: model.start, take: model.length)
                .Select(q => new ModuleViewModel
                {
                    Module = q.Module,
                    Actions = DisplayActions(UserAuthorization, q.ModuleId, submenus)
                })
                .ToList();

            var totalResultsCount = _moduleServices.Get().Count();
            var recordsFiltered = String.IsNullOrEmpty(model.search.value) ? totalResultsCount : _moduleServices.Get(filter: filter).Count();

            return Json(new
            {
                draw = model.draw,
                recordsTotal = totalResultsCount,
                recordsFiltered = recordsFiltered,
                data = models.ToList()
            });
        }

        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Add(ModuleAddForm form)
        {
            if (ModelState.IsValid)
            {
                var action = _moduleServices.AddModule(form, UserAuthorized);
                if (action.IsSuccess)
                {
                    return RedirectToAction("Index");
                }

                var message = DisplayMessage(action.Message, new string[] { "Module" });
                ModelState.AddModelError("", message);
            }
            return View(form);
        }

        public ActionResult Edit(int Id)
        {
            var model = _moduleServices.GetModuleById(Id);
            if (model == null)
            {
                return RedirectToAction("Index", "NotFound");
            }

            var form = new ModuleEditForm();
            form.ModuleId = model.ModuleId;
            form.Module = model.Module;
            form.Note = model.Note;

            return View(form);
        }

        [HttpPost]
        public ActionResult Edit(ModuleEditForm form)
        {
            if (ModelState.IsValid)
            {
                var action = _moduleServices.UpdateModule(form, UserAuthorized);
                if (action.IsSuccess)
                {
                    return RedirectToAction("Index");
                }

                var message = DisplayMessage(action.Message, new string[] { "Module" });
                ModelState.AddModelError("", message);
            }

            return View(form);
        }

        public ActionResult Detail(int Id)
        {
            var model = _moduleServices.GetModuleById(Id);
            if (model == null)
            {
                return RedirectToAction("Index", "NotFound");
            }

            var viewModel = new ModuleViewModel();
            viewModel.ModuleId = model.ModuleId;
            viewModel.Module = model.Module;
            viewModel.Note = model.Note;

            return View(viewModel);
        }

        public ActionResult Delete(int Id)
        {
            var model = _moduleServices.GetModuleById(Id);
            if (model == null)
            {
                return RedirectToAction("Index", "NotFound");
            }

            var form = new ModuleDeleteForm();
            form.ModuleId = model.ModuleId;
            form.Module = model.Module;
            form.Note = model.Note;

            return View(form);
        }

        [HttpPost]
        public ActionResult Delete(ModuleDeleteForm form)
        {
            if (ModelState.IsValid)
            {
                var action = _moduleServices.DeleteModule(form, UserAuthorized);
                if (action.IsSuccess)
                {
                    return RedirectToAction("Index");
                }
                var message = DisplayMessage(action.Message, null);
                ModelState.AddModelError("", action.Message);
            }

            return View(form);
        }
    }
}