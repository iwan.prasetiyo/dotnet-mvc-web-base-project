﻿using FQ.MD.WEB.ViewModels.JSON;
using FQ.MD.WEB.Helpers;
using FQ.MD.WEB.Models;
using FQ.MD.WEB.Resources;
using FQ.MD.WEB.Services;
using FQ.MD.WEB.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;

namespace FQ.MD.WEB.Controllers
{
    [Authorize]
    public class CompaniesController : BaseController
    {
        private CompanyServices _services = new CompanyServices();

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Index(DataTables model)
        {
            var searchKeywords = !String.IsNullOrEmpty(model.search.value) ? model.search.value.ToLower() : "";

            string sortBy = "CompanyName";
            bool sortDir = true;

            if (model.order != null)
            {
                sortBy = model.columns[model.order[0].column].data;
                sortDir = model.order[0].dir.ToLower() == "asc";
            }

            Expression<Func<GT_MASTER_COMPANIES, bool>> filter = u => u.CompanyName.ToLower().Contains(searchKeywords);
            Func<IQueryable<GT_MASTER_COMPANIES>, IOrderedQueryable<GT_MASTER_COMPANIES>> orderBy = q => sortDir ? q.OrderBy(u => u.CompanyName) : q.OrderByDescending(u => u.CompanyName);

            if (sortBy == "Active")
            {
                orderBy = q => sortDir ? q.OrderBy(u => u.IsActive) : q.OrderByDescending(u => u.IsActive);
            }

            var models = _services.Get(filter: filter, orderBy: orderBy, start: model.start, take: model.length)
                .Select(c => new CompanyViewModel
                {
                    CompanyName = c.CompanyName,
                    Active = DisplayActiveIcon(c.IsActive),
                    Actions = DisplayActions(UserAuthorization, c.CompanyId, null)
                });

            var totalResultsCount = _services.Get().Count();
            var recordsFiltered = String.IsNullOrEmpty(model.search.value) ? totalResultsCount : _services.Get(filter: filter).Count();

            return Json(new
            {
                draw = model.draw,
                recordsTotal = totalResultsCount,
                recordsFiltered = recordsFiltered,
                data = models.ToList()
            });
        }

        public ActionResult Add()
        {
            return View();
        }

        public ActionResult Edit(int Id)
        {
            var model = _services.GetCompanyById(Id);
            if (model == null)
            {
                return RedirectToAction("Index", "NotFound");
            }

            var viewModel = new CompanyViewModel();
            viewModel.CompanyId = model.CompanyId;
            viewModel.CompanyName = model.CompanyName;
            viewModel.CompanyPhone = model.CompanyPhone;
            viewModel.CompanyEmail = model.CompanyEmail;
            viewModel.CompanyWebsite = model.CompanyWebsite;
            viewModel.IsActive = model.IsActive;

            return View(viewModel);
        }

        public ActionResult Detail(int Id)
        {
            var model = _services.GetCompanyById(Id);
            if (model == null)
            {
                return RedirectToAction("Index", "NotFound");
            }

            var viewModel = new CompanyViewModel();
            viewModel.CompanyId = model.CompanyId;
            viewModel.CompanyName = model.CompanyName;
            viewModel.CompanyPhone = model.CompanyPhone;
            viewModel.CompanyEmail = model.CompanyEmail;
            viewModel.CompanyWebsite = model.CompanyWebsite;
            viewModel.IsActive = model.IsActive;

            return View(viewModel);
        }

        public ActionResult Delete(int Id)
        {
            var model = _services.GetCompanyById(Id);
            if (model == null)
            {
                return RedirectToAction("Index", "NotFound");
            }

            var viewModel = new CompanyViewModel();
            viewModel.CompanyId = model.CompanyId;
            viewModel.CompanyName = model.CompanyName;
            viewModel.CompanyPhone = model.CompanyPhone;
            viewModel.CompanyEmail = model.CompanyEmail;
            viewModel.CompanyWebsite = model.CompanyWebsite;
            viewModel.IsActive = model.IsActive;

            return View(viewModel);
        }
    }
}