﻿using FQ.MD.WEB.ViewModels.JSON;
using FQ.MD.WEB.Helpers;
using FQ.MD.WEB.Models;
using FQ.MD.WEB.Resources;
using FQ.MD.WEB.Services;
using FQ.MD.WEB.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web;
using System.Web.Mvc;
using FQ.MD.WEB.ViewModels.Form;

namespace FQ.MD.WEB.Controllers
{
    [Authorize]
    public class UsersController : BaseController
    {
        private UserServices _userServices = new UserServices();
        private GroupServices _groupServices = new GroupServices();

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Index(DataTables model)
        {
            var searchKeywords = !String.IsNullOrEmpty(model.search.value) ? model.search.value.ToLower() : "";

            string sortBy = "Name";
            bool sortDir = true;

            if (model.order != null)
            {
                sortBy = model.columns[model.order[0].column].data;
                sortDir = model.order[0].dir.ToLower() == "asc";
            }

            Expression<Func<GT_MASTER_USERS, bool>> filter = u => u.Username.ToLower().Contains(searchKeywords) || u.Name.ToLower().Contains(searchKeywords) || u.GT_MASTER_GROUPS.GroupName.ToLower().Contains(searchKeywords);
            Func<IQueryable<GT_MASTER_USERS>, IOrderedQueryable<GT_MASTER_USERS>> orderBy = q => sortDir ? q.OrderBy(u => u.Username) : q.OrderByDescending(u => u.Username);
            if (sortBy == "Name")
            {
                orderBy = q => sortDir ? q.OrderBy(u => u.Name) : q.OrderByDescending(u => u.Name);
            }
            if (sortBy == "GroupName")
            {
                orderBy = q => sortDir ? q.OrderBy(u => u.GT_MASTER_GROUPS.GroupName) : q.OrderByDescending(u => u.GT_MASTER_GROUPS.GroupName);
            }
            if (sortBy == "Active")
            {
                orderBy = q => sortDir ? q.OrderBy(u => u.IsActive) : q.OrderByDescending(u => u.IsActive);
            }

            var submenus = new List<ActionsSubmenu>() { new ActionsSubmenu() { SubmenuLabel = "Roles", SubmenuKey = "UserId", SubmenuController = "UsersRoles" } };

            var models = _userServices.Get(filter: filter, orderBy: orderBy, skip: model.start, take: model.length)
                .Select(u => new UserViewModel
                {
                    Username = u.Username,
                    Name = u.Name,
                    GroupName = u.GT_MASTER_GROUPS.GroupName,
                    Active = DisplayActiveIcon(u.IsActive),
                    Actions = DisplayActions(UserAuthorization, u.UserId, submenus)
                });

            var totalResultsCount = _userServices.Get().Count();
            var recordsFiltered = String.IsNullOrEmpty(model.search.value) ? totalResultsCount : _userServices.Get(filter: filter).Count();

            return Json(new
            {
                draw = model.draw,
                recordsTotal = totalResultsCount,
                recordsFiltered = recordsFiltered,
                data = models.ToList()
            });
        }

        public ActionResult Add()
        {
            ViewBag.GroupId = GroupOptions(null);

            return View();
        }

        [HttpPost]
        public ActionResult Add(UserAddForm form)
        {
            if (ModelState.IsValid)
            {
                var action = _userServices.AddUser(form, UserAuthorized);
                if (action.IsSuccess)
                {
                    return RedirectToAction("Index");
                }

                var message = DisplayMessage(action.Message, new string[] { "Username", "Email" });
                ModelState.AddModelError("", message);
            }

            ViewBag.GroupId = GroupOptions(form.GroupId);

            return View(form);
        }

        public ActionResult Edit(int Id)
        {
            var model = _userServices.GetUserById(Id);
            if (model == null)
            {
                return RedirectToAction("Index", "NotFound");
            }

            var form = new UserEditForm();
            form.UserId = model.UserId;
            form.Username = model.Username;
            form.Name = model.Name;
            form.Email = model.Email;
            form.GroupId = model.GroupId;
            form.IsActive = model.IsActive;

            ViewBag.GroupId = GroupOptions(form.GroupId);

            return View(form);
        }

        [HttpPost]
        public ActionResult Edit(UserEditForm form)
        {
            if (ModelState.IsValid)
            {
                var action = _userServices.UpdateUser(form, UserAuthorized);
                if (action.IsSuccess)
                {
                    return RedirectToAction("Index");
                }
            }

            ViewBag.GroupId = GroupOptions(form.GroupId);
            var user = _userServices.GetUserById(form.UserId);

            return View(form);
        }

        public ActionResult Detail(int Id)
        {
            var model = _userServices.GetUserById(Id);
            if (model == null)
            {
                return RedirectToAction("Index", "NotFound");
            }

            var viewModel = new UserViewModel();
            viewModel.UserId = model.UserId;
            viewModel.Username = model.Username;
            viewModel.Name = model.Name;
            viewModel.Email = model.Email;
            viewModel.GroupName = model.GT_MASTER_GROUPS.GroupName;
            viewModel.Active = DisplayActiveIcon(model.IsActive);

            return View(viewModel);
        }

        public ActionResult Delete(int Id)
        {
            var model = _userServices.GetUserById(Id);
            if (model == null)
            {
                return RedirectToAction("Index", "NotFound");
            }

            var form = new UserDeleteForm();
            form.UserId = model.UserId;
            form.Username = model.Username;
            form.Name = model.Name;
            form.Email = model.Email;
            form.GroupId = model.GroupId;

            ViewBag.GroupId = GroupOptions(form.GroupId);

            return View(form);
        }

        [HttpPost]
        public ActionResult Delete(UserDeleteForm form)
        {
            if (ModelState.IsValid)
            {
                var action = _userServices.DeleteUser(form, UserAuthorized);
                if (action.IsSuccess)
                {
                    return RedirectToAction("Index");
                }

                var message = DisplayMessage(action.Message, null);
                ModelState.AddModelError("", message);
            }

            ViewBag.GroupId = GroupOptions(form.GroupId);

            return View(form);
        }

        public ActionResult EditPassword(int Id)
        {
            var model = _userServices.GetUserById(Id);
            if (model == null)
            {
                return RedirectToAction("Index", "NotFound");
            }

            var form = new UserChangePasswordForm();
            form.UserId = model.UserId;

            ViewBag.Name = model.Name;
            ViewBag.Group = model.GT_MASTER_GROUPS.GroupName;

            return View(form);
        }

        [HttpPost]
        public ActionResult EditPassword(UserChangePasswordForm form)
        {
            if (ModelState.IsValid)
            {
                var action = _userServices.ChangeUserPassword(form.UserId, form.Password, UserAuthorized.UserId);
                if (action.IsSuccess)
                {
                    return RedirectToAction("Edit", new { Id = form.UserId });
                }

                var message = DisplayMessage(action.Message, null);
                ModelState.AddModelError("", message);
            }

            var user = _userServices.GetUserById(form.UserId);
            ViewBag.Name = user.Username;
            ViewBag.GroupName = user.GT_MASTER_GROUPS.GroupName;

            return View(form);
        }

        private SelectList GroupOptions(int? GroupId)
        {
            var Groups = _groupServices.Get(filter: q => q.IsActive).Select(g => new SelectItemIntValue
            {
                Value = g.GroupId,
                Text = g.GT_MASTER_COMPANIES.CompanyName + " - " + g.GroupName
            }).ToList();
            Groups.Insert(0, new SelectItemIntValue() { Value = null, Text = Res.SelectItemNull });

            return new SelectList(Groups, "Value", "Text", GroupId);
        }
    }
}