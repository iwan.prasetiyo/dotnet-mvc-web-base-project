﻿using FQ.MD.WEB.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FQ.MD.WEB.Controllers
{
    public class ServerErrorController : BaseController
    {
        // GET: ServerError
        public ActionResult Index(string DisplayKey)
        {
            ViewBag.Message = CultureHelper.GetResourceByCultureId(DisplayKey, CultureHelper.CurrentCulture);
            return View();
        }
    }
}