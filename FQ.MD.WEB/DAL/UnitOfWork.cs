﻿using FQ.MD.WEB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FQ.MD.WEB.DAL
{
    public class UnitOfWork : IDisposable
    {
        private Entities context = new Entities();

        private GenericRepository<GT_LOG_APP_ERRORS> logErrorRepository;
        private GenericRepository<GT_LOG_WEB_ACTIVITIES> logWebActivityRepository;
        private GenericRepository<GT_MASTER_COMPANIES> companyRepository;
        private GenericRepository<GT_MASTER_GROUPS> groupRepository;
        private GenericRepository<GT_MASTER_MENUS> menuRepository;
        private GenericRepository<GT_MASTER_ROLE_MENUS> roleMenuRepository;
        private GenericRepository<GT_MASTER_ROLES> roleRepository;
        private GenericRepository<GT_MASTER_USER_ROLES> userRoleRepository;
        private GenericRepository<GT_MASTER_USERS> userRepository;
        private GenericRepository<GT_MASTER_MODULES> moduleRepository;
        private GenericRepository<GT_MASTER_STATUS> statusRepository;

        public GenericRepository<GT_LOG_WEB_ACTIVITIES> LogWebActivityRepository
        {
            get
            {

                if (this.logWebActivityRepository == null)
                {
                    this.logWebActivityRepository = new GenericRepository<GT_LOG_WEB_ACTIVITIES>(context);
                }
                return logWebActivityRepository;
            }
        }

        public GenericRepository<GT_LOG_APP_ERRORS> LogErrorRepository
        {
            get
            {

                if (this.logErrorRepository == null)
                {
                    this.logErrorRepository = new GenericRepository<GT_LOG_APP_ERRORS>(context);
                }
                return logErrorRepository;
            }
        }

        public GenericRepository<GT_MASTER_COMPANIES> CompanyRepository
        {
            get
            {

                if (this.companyRepository == null)
                {
                    this.companyRepository = new GenericRepository<GT_MASTER_COMPANIES>(context);
                }
                return companyRepository;
            }
        }

        public GenericRepository<GT_MASTER_GROUPS> GroupRepository
        {
            get
            {

                if (this.groupRepository == null)
                {
                    this.groupRepository = new GenericRepository<GT_MASTER_GROUPS>(context);
                }
                return groupRepository;
            }
        }

        public GenericRepository<GT_MASTER_MENUS> MenuRepository
        {
            get
            {

                if (this.menuRepository == null)
                {
                    this.menuRepository = new GenericRepository<GT_MASTER_MENUS>(context);
                }
                return menuRepository;
            }
        }

        public GenericRepository<GT_MASTER_ROLE_MENUS> RoleMenuRepository
        {
            get
            {

                if (this.roleMenuRepository == null)
                {
                    this.roleMenuRepository = new GenericRepository<GT_MASTER_ROLE_MENUS>(context);
                }
                return roleMenuRepository;
            }
        }

        public GenericRepository<GT_MASTER_ROLES> RoleRepository
        {
            get
            {

                if (this.roleRepository == null)
                {
                    this.roleRepository = new GenericRepository<GT_MASTER_ROLES>(context);
                }
                return roleRepository;
            }
        }

        public GenericRepository<GT_MASTER_USER_ROLES> UserRoleRepository
        {
            get
            {

                if (this.userRoleRepository == null)
                {
                    this.userRoleRepository = new GenericRepository<GT_MASTER_USER_ROLES>(context);
                }
                return userRoleRepository;
            }
        }

        public GenericRepository<GT_MASTER_USERS> UserRepository
        {
            get
            {

                if (this.userRepository == null)
                {
                    this.userRepository = new GenericRepository<GT_MASTER_USERS>(context);
                }
                return userRepository;
            }
        }

        public GenericRepository<GT_MASTER_MODULES> ModuleRepository
        {
            get
            {
                if (this.moduleRepository == null)
                {
                    this.moduleRepository = new GenericRepository<GT_MASTER_MODULES>(context);
                }
                return moduleRepository;
            }
        }

        public GenericRepository<GT_MASTER_STATUS> StatusRepository
        {
            get
            {
                if (this.statusRepository == null)
                {
                    this.statusRepository = new GenericRepository<GT_MASTER_STATUS>(context);
                }
                return statusRepository;
            }
        }

        public void Save()
        {
            context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
