# .NET MVC WEB BASE PROJECT
MVC web base project using .Net Framework 4.5.2

# FEATURES
1. RBAC
2. Repository Pattern
3. Access & Error Log
4. Dataseeder
5. Multilaguage
6. Included Libraries :
    * [Bootstrap 4.4.1 ](https://getbootstrap.com)
    * [jQuery 3.0.0](https://jquery.com)
    * [DataTables 1.10.15](https://datatables.net)
    * [Select2 4.0.11](https://select2.org)
    * [Font Awesome Free 5.12](https://fontawesome.com)
    * [Chart.js v2.9.3](https://www.chartjs.org)
    * [MultiDatesPicker v1.6.6](https://dubrox.github.io/Multiple-Dates-Picker-for-jQuery-UI/)

# HOW TO USE
1. Dowload template [URL](/uploads/b07f988dd4fa1b505767047287e51dba/FQ.MD.WEB.zip)
2. Move downloaded template to C:\Users\ *username* \Documents\Visual Studio 2013\Templates\ProjectTemplates\Visual C#
3. Open VS 2013 and create New Project
4. Choose Templates \ Visual C# \ FQ.MD.WEB
![Choose Templates \ Visual C# \ FQ.MD.WEB](/uploads/7de6e03043e617c8f413d87f31035487/new_project_template_screen.PNG)
5. Rename project name
6. Open SQL folder and create database by execute `BASE.sql` (rename database)
7. Rebuild Project
8. Run Project

# ENVIRONMENT
Visual Studio 2013 Community\
.Net Framework 4.5.2\
LocalDB

# Author
[faqih-md](https://faqih-md.web.app)